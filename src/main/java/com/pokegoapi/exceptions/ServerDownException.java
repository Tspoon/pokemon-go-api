package com.pokegoapi.exceptions;

public class ServerDownException extends Exception {

    public ServerDownException(Throwable cause) {
        super(cause);
    }
}
