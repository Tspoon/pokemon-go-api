package com.pokegoapi.exceptions;

public class LoginFailedException extends Exception {

    public static final int AUTH_TOKEN_ERROR_CODE = 102;

    private int mErrorCode = -1;

    public LoginFailedException(Throwable cause) {
        super(cause);
    }

    public LoginFailedException(String message) {
        super(message);
    }

    public LoginFailedException(int errorCode) {
        mErrorCode = errorCode;
    }

    public int getErrorCode() {
        return mErrorCode;
    }
}
