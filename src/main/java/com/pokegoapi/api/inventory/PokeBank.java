package com.pokegoapi.api.inventory;

import POGOProtos.Enums.PokemonIdOuterClass;
import com.pokegoapi.api.PokemonGo;

import java.util.ArrayList;
import java.util.List;


public class PokeBank {
	List<Pokemon> pokemons = new ArrayList<Pokemon>();
	PokemonGo instance;

	public PokeBank(PokemonGo instance) {
		this.instance = instance;
	}

    public List<Pokemon> getPokemons() {
        return pokemons;
    }

    public PokemonGo getInstance() {
        return instance;
    }

    public void addPokemon(Pokemon pokemon) {
		pokemon.setPgo(instance);
		pokemons.add(pokemon);
	}

	public List<Pokemon> getPokemonByPokemonId(final PokemonIdOuterClass.PokemonId id) {
		ArrayList<Pokemon> list = new ArrayList<>();
		for (Pokemon pokemon: pokemons) {
			if(pokemon.getPokemonId().equals(id)) {
				list.add(pokemon);
			}
		}
		return list;
	}

	public void removePokemon(final Pokemon pokemon) {
		for (int i = 0; i < pokemons.size(); i++) {
			Pokemon p = pokemons.get(i);
			if(pokemon.getId() == p.getId()) {
				pokemons.remove(i);
			}
		}
	}
}
