package com.pokegoapi.api.player;


public class ContactSettings {
	
	private boolean sendMarketingEmails;
	private boolean sendPushNotifications;

	public boolean isSendMarketingEmails() {
		return sendMarketingEmails;
	}

	public boolean isSendPushNotifications() {
		return sendPushNotifications;
	}
}
