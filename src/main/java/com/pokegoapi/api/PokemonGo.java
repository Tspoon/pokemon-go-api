package com.pokegoapi.api;


import com.pokegoapi.api.inventory.Bag;
import com.pokegoapi.api.inventory.Item;
import com.pokegoapi.api.inventory.PokeBank;
import com.pokegoapi.api.inventory.Pokemon;
import com.pokegoapi.api.map.Map;
import com.pokegoapi.api.player.ContactSettings;
import com.pokegoapi.api.player.DailyBonus;
import com.pokegoapi.api.player.PlayerAvatar;
import com.pokegoapi.api.player.PlayerProfile;
import com.pokegoapi.api.player.Team;
import com.pokegoapi.exceptions.InvalidCurrencyException;
import com.pokegoapi.main.RequestHandler;
import com.pokegoapi.main.ServerRequest;

import POGOProtos.Enums.PokemonIdOuterClass;
import POGOProtos.Inventory.InventoryItemOuterClass;
import POGOProtos.Inventory.ItemIdOuterClass;
import POGOProtos.LocalPlayerOuterClass;
import POGOProtos.Networking.EnvelopesOuterClass;
import POGOProtos.Networking.Requests.Messages.GetInventoryMessageOuterClass.GetInventoryMessage;
import POGOProtos.Networking.Requests.Messages.GetPlayerMessageOuterClass.GetPlayerMessage;
import POGOProtos.Networking.Requests.Messages.PlayerUpdateMessageOuterClass;
import POGOProtos.Networking.Requests.RequestTypeOuterClass.RequestType;
import POGOProtos.Networking.Responses.GetInventoryResponseOuterClass.GetInventoryResponse;
import POGOProtos.Networking.Responses.GetPlayerResponseOuterClass.GetPlayerResponse;
import POGOProtos.Networking.Responses.PlayerUpdateResponseOuterClass;
import POGOProtos.Player.CurrencyOuterClass.Currency;
import okhttp3.OkHttpClient;

import static POGOProtos.Networking.Requests.Messages.PlayerUpdateMessageOuterClass.*;
import static POGOProtos.Networking.Responses.PlayerUpdateResponseOuterClass.*;

public class PokemonGo {

    RequestHandler requestHandler;
    private PlayerProfile playerProfile;
    PokeBank pokebank;
    Bag bag;
    Map map;
    //private PlayerUpdateMessage playerUpdate;

    private double latitude;
    private double longitude;
    private double altitude;

    private long lastInventoryUpdate;

    public PokemonGo(EnvelopesOuterClass.Envelopes.RequestEnvelope.AuthInfo auth, OkHttpClient client) {
        playerProfile = null;

        // send profile request to get the ball rolling
        requestHandler = new RequestHandler(this, auth, client);

        pokebank = new PokeBank(this);
        bag = new Bag(this);
        map = new Map(this);
        lastInventoryUpdate = 0;
    }

    public RequestHandler getRequestHandler() {
        return requestHandler;
    }

    public PokeBank getPokebank() {
        return pokebank;
    }

    public Map getMap() {
        return map;
    }

    public Bag getBag() {
        return bag;
    }

    public PlayerProfile getPlayerProfile() {
        return playerProfile;
    }

    public long getLastInventoryUpdate() {
        return lastInventoryUpdate;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    /*
    private PlayerUpdateResponse getPlayerUpdate() {


        // server request
        try {
            PlayerUpdateMessage reqMsg = PlayerUpdateMessage.newBuilder().build();
            ServerRequest serverRequest = new ServerRequest(RequestType.PLAYER_UPDATE, reqMsg);
            requestHandler.request(serverRequest);
            requestHandler.sendServerRequests();
            return PlayerUpdateResponse.parseFrom(serverRequest.getData());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    */

    private LocalPlayerOuterClass.LocalPlayer getLocalPlayer() {
        LocalPlayerOuterClass.LocalPlayer localPlayer = null;

        // server request
        try {
            GetPlayerMessage reqMsg = GetPlayerMessage.newBuilder().build();
            ServerRequest serverRequest = new ServerRequest(RequestType.GET_PLAYER, reqMsg);
            requestHandler.request(serverRequest);
            requestHandler.sendServerRequests();
            GetPlayerResponse response = GetPlayerResponse.parseFrom(serverRequest.getData());
            localPlayer = response.getLocalPlayer();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

        return localPlayer;
    }

    public PlayerProfile fetchPlayerProfile() {
        playerProfile = new PlayerProfile();

        LocalPlayerOuterClass.LocalPlayer localPlayer = getLocalPlayer();

        if (localPlayer == null) {
            return null;
        }

        playerProfile.setBadge(localPlayer.getEquippedBadge());
        playerProfile.setCreationTime(localPlayer.getCreationTimestampMs());
        playerProfile.setItemStorage(localPlayer.getMaxItemStorage());
        playerProfile.setPokemonStorage(localPlayer.getMaxPokemonStorage());
        playerProfile.setTeam(Team.values()[localPlayer.getTeam()]);
        playerProfile.setUsername(localPlayer.getUsername());

        PlayerAvatar avatarAPI = new PlayerAvatar();
        DailyBonus bonusAPI = new DailyBonus();
        ContactSettings contactAPI = new ContactSettings();

        // maybe something more graceful?
        for (Currency currency : localPlayer.getCurrenciesList()) {
            try {
                playerProfile.addCurrency(currency.getName(), currency.getAmount());
            } catch (InvalidCurrencyException e) {
                throw new IllegalStateException(e);
            }
        }

        avatarAPI.setGender(localPlayer.getAvatarDetails().getGender());
        avatarAPI.setBackpack(localPlayer.getAvatarDetails().getBackpack());
        avatarAPI.setEyes(localPlayer.getAvatarDetails().getEyes());
        avatarAPI.setHair(localPlayer.getAvatarDetails().getHair());
        avatarAPI.setHat(localPlayer.getAvatarDetails().getHat());
        avatarAPI.setPants(localPlayer.getAvatarDetails().getPants());
        avatarAPI.setShirt(localPlayer.getAvatarDetails().getShirt());
        avatarAPI.setShoes(localPlayer.getAvatarDetails().getShoes());
        avatarAPI.setSkin(localPlayer.getAvatarDetails().getSkin());

        bonusAPI.setNextCollectionTimestamp(localPlayer.getDailyBonus().getNextCollectedTimestampMs());
        bonusAPI.setNextDefenderBonusCollectTimestamp(localPlayer.getDailyBonus().getNextDefenderBonusCollectTimestampMs());

        playerProfile.setAvatar(avatarAPI);
        playerProfile.setDailyBonus(bonusAPI);

        return playerProfile;
    }

    public void fetchInventory() {

        // server request
        try {
            GetInventoryMessage reqMsg = GetInventoryMessage.newBuilder()
                    .setLastTimestampMs(this.lastInventoryUpdate)
                    .build();
            ServerRequest serverRequest = new ServerRequest(RequestType.GET_INVENTORY, reqMsg);
            requestHandler.request(serverRequest);
            requestHandler.sendServerRequests();
            GetInventoryResponse response = GetInventoryResponse.parseFrom(serverRequest.getData());

            for (InventoryItemOuterClass.InventoryItem item : response.getInventoryDelta().getInventoryItemsList()) {

                if (item.getInventoryItemData().getPokemonData().getPokemonId() != PokemonIdOuterClass.PokemonId.MISSINGNO) {
                    pokebank.addPokemon(new Pokemon(item.getInventoryItemData().getPokemonData()));
                }

                if (item.getInventoryItemData().getItem().getItemId() != ItemIdOuterClass.ItemId.ITEM_UNKNOWN) {
                    bag.addItem(new Item(item.getInventoryItemData().getItem()));
                }

            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
