package com.pokegoapi.main;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Utils {
	public static ObjectMapper getObjectMapper() {
		return new ObjectMapper();
	}
}
