// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Settings/DownloadSettingsAction.proto

package POGOProtos.Settings;

public final class DownloadSettingsActionOuterClass {
  private DownloadSettingsActionOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface DownloadSettingsActionOrBuilder extends
      // @@protoc_insertion_point(interface_extends:POGOProtos.Settings.DownloadSettingsAction)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>optional string hash = 1;</code>
     */
    java.lang.String getHash();
    /**
     * <code>optional string hash = 1;</code>
     */
    com.google.protobuf.ByteString
        getHashBytes();
  }
  /**
   * Protobuf type {@code POGOProtos.Settings.DownloadSettingsAction}
   */
  public  static final class DownloadSettingsAction extends
      com.google.protobuf.GeneratedMessage implements
      // @@protoc_insertion_point(message_implements:POGOProtos.Settings.DownloadSettingsAction)
      DownloadSettingsActionOrBuilder {
    // Use DownloadSettingsAction.newBuilder() to construct.
    private DownloadSettingsAction(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
    }
    private DownloadSettingsAction() {
      hash_ = "";
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private DownloadSettingsAction(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 10: {
              java.lang.String s = input.readStringRequireUtf8();

              hash_ = s;
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return POGOProtos.Settings.DownloadSettingsActionOuterClass.internal_static_POGOProtos_Settings_DownloadSettingsAction_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return POGOProtos.Settings.DownloadSettingsActionOuterClass.internal_static_POGOProtos_Settings_DownloadSettingsAction_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction.class, POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction.Builder.class);
    }

    public static final int HASH_FIELD_NUMBER = 1;
    private volatile java.lang.Object hash_;
    /**
     * <code>optional string hash = 1;</code>
     */
    public java.lang.String getHash() {
      java.lang.Object ref = hash_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        hash_ = s;
        return s;
      }
    }
    /**
     * <code>optional string hash = 1;</code>
     */
    public com.google.protobuf.ByteString
        getHashBytes() {
      java.lang.Object ref = hash_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        hash_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (!getHashBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessage.writeString(output, 1, hash_);
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (!getHashBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessage.computeStringSize(1, hash_);
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code POGOProtos.Settings.DownloadSettingsAction}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:POGOProtos.Settings.DownloadSettingsAction)
        POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsActionOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return POGOProtos.Settings.DownloadSettingsActionOuterClass.internal_static_POGOProtos_Settings_DownloadSettingsAction_descriptor;
      }

      protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return POGOProtos.Settings.DownloadSettingsActionOuterClass.internal_static_POGOProtos_Settings_DownloadSettingsAction_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction.class, POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction.Builder.class);
      }

      // Construct using POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessage.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        }
      }
      public Builder clear() {
        super.clear();
        hash_ = "";

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return POGOProtos.Settings.DownloadSettingsActionOuterClass.internal_static_POGOProtos_Settings_DownloadSettingsAction_descriptor;
      }

      public POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction getDefaultInstanceForType() {
        return POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction.getDefaultInstance();
      }

      public POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction build() {
        POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction buildPartial() {
        POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction result = new POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction(this);
        result.hash_ = hash_;
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction) {
          return mergeFrom((POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction other) {
        if (other == POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction.getDefaultInstance()) return this;
        if (!other.getHash().isEmpty()) {
          hash_ = other.hash_;
          onChanged();
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private java.lang.Object hash_ = "";
      /**
       * <code>optional string hash = 1;</code>
       */
      public java.lang.String getHash() {
        java.lang.Object ref = hash_;
        if (!(ref instanceof java.lang.String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          java.lang.String s = bs.toStringUtf8();
          hash_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>optional string hash = 1;</code>
       */
      public com.google.protobuf.ByteString
          getHashBytes() {
        java.lang.Object ref = hash_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          hash_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>optional string hash = 1;</code>
       */
      public Builder setHash(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        hash_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional string hash = 1;</code>
       */
      public Builder clearHash() {
        
        hash_ = getDefaultInstance().getHash();
        onChanged();
        return this;
      }
      /**
       * <code>optional string hash = 1;</code>
       */
      public Builder setHashBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        hash_ = value;
        onChanged();
        return this;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:POGOProtos.Settings.DownloadSettingsAction)
    }

    // @@protoc_insertion_point(class_scope:POGOProtos.Settings.DownloadSettingsAction)
    private static final POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction();
    }

    public static POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<DownloadSettingsAction>
        PARSER = new com.google.protobuf.AbstractParser<DownloadSettingsAction>() {
      public DownloadSettingsAction parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new DownloadSettingsAction(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<DownloadSettingsAction> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<DownloadSettingsAction> getParserForType() {
      return PARSER;
    }

    public POGOProtos.Settings.DownloadSettingsActionOuterClass.DownloadSettingsAction getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_POGOProtos_Settings_DownloadSettingsAction_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_POGOProtos_Settings_DownloadSettingsAction_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n%Settings/DownloadSettingsAction.proto\022" +
      "\023POGOProtos.Settings\"&\n\026DownloadSettings" +
      "Action\022\014\n\004hash\030\001 \001(\tb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_POGOProtos_Settings_DownloadSettingsAction_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_POGOProtos_Settings_DownloadSettingsAction_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessage.FieldAccessorTable(
        internal_static_POGOProtos_Settings_DownloadSettingsAction_descriptor,
        new java.lang.String[] { "Hash", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
