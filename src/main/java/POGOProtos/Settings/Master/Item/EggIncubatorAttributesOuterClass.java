// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Settings/Master/Item/EggIncubatorAttributes.proto

package POGOProtos.Settings.Master.Item;

public final class EggIncubatorAttributesOuterClass {
  private EggIncubatorAttributesOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface EggIncubatorAttributesOrBuilder extends
      // @@protoc_insertion_point(interface_extends:POGOProtos.Settings.Master.Item.EggIncubatorAttributes)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>optional .POGOProtos.Inventory.EggIncubatorType incubator_type = 1;</code>
     */
    int getIncubatorTypeValue();
    /**
     * <code>optional .POGOProtos.Inventory.EggIncubatorType incubator_type = 1;</code>
     */
    POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType getIncubatorType();

    /**
     * <code>optional int32 uses = 2;</code>
     */
    int getUses();

    /**
     * <code>optional float distance_multiplier = 3;</code>
     */
    float getDistanceMultiplier();
  }
  /**
   * Protobuf type {@code POGOProtos.Settings.Master.Item.EggIncubatorAttributes}
   */
  public  static final class EggIncubatorAttributes extends
      com.google.protobuf.GeneratedMessage implements
      // @@protoc_insertion_point(message_implements:POGOProtos.Settings.Master.Item.EggIncubatorAttributes)
      EggIncubatorAttributesOrBuilder {
    // Use EggIncubatorAttributes.newBuilder() to construct.
    private EggIncubatorAttributes(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
    }
    private EggIncubatorAttributes() {
      incubatorType_ = 0;
      uses_ = 0;
      distanceMultiplier_ = 0F;
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private EggIncubatorAttributes(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 8: {
              int rawValue = input.readEnum();

              incubatorType_ = rawValue;
              break;
            }
            case 16: {

              uses_ = input.readInt32();
              break;
            }
            case 29: {

              distanceMultiplier_ = input.readFloat();
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.internal_static_POGOProtos_Settings_Master_Item_EggIncubatorAttributes_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.internal_static_POGOProtos_Settings_Master_Item_EggIncubatorAttributes_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes.class, POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes.Builder.class);
    }

    public static final int INCUBATOR_TYPE_FIELD_NUMBER = 1;
    private int incubatorType_;
    /**
     * <code>optional .POGOProtos.Inventory.EggIncubatorType incubator_type = 1;</code>
     */
    public int getIncubatorTypeValue() {
      return incubatorType_;
    }
    /**
     * <code>optional .POGOProtos.Inventory.EggIncubatorType incubator_type = 1;</code>
     */
    public POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType getIncubatorType() {
      POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType result = POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType.forNumber(incubatorType_);
      return result == null ? POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType.UNRECOGNIZED : result;
    }

    public static final int USES_FIELD_NUMBER = 2;
    private int uses_;
    /**
     * <code>optional int32 uses = 2;</code>
     */
    public int getUses() {
      return uses_;
    }

    public static final int DISTANCE_MULTIPLIER_FIELD_NUMBER = 3;
    private float distanceMultiplier_;
    /**
     * <code>optional float distance_multiplier = 3;</code>
     */
    public float getDistanceMultiplier() {
      return distanceMultiplier_;
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (incubatorType_ != POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType.INCUBATOR_UNSET.getNumber()) {
        output.writeEnum(1, incubatorType_);
      }
      if (uses_ != 0) {
        output.writeInt32(2, uses_);
      }
      if (distanceMultiplier_ != 0F) {
        output.writeFloat(3, distanceMultiplier_);
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (incubatorType_ != POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType.INCUBATOR_UNSET.getNumber()) {
        size += com.google.protobuf.CodedOutputStream
          .computeEnumSize(1, incubatorType_);
      }
      if (uses_ != 0) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt32Size(2, uses_);
      }
      if (distanceMultiplier_ != 0F) {
        size += com.google.protobuf.CodedOutputStream
          .computeFloatSize(3, distanceMultiplier_);
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code POGOProtos.Settings.Master.Item.EggIncubatorAttributes}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:POGOProtos.Settings.Master.Item.EggIncubatorAttributes)
        POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributesOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.internal_static_POGOProtos_Settings_Master_Item_EggIncubatorAttributes_descriptor;
      }

      protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.internal_static_POGOProtos_Settings_Master_Item_EggIncubatorAttributes_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes.class, POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes.Builder.class);
      }

      // Construct using POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessage.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        }
      }
      public Builder clear() {
        super.clear();
        incubatorType_ = 0;

        uses_ = 0;

        distanceMultiplier_ = 0F;

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.internal_static_POGOProtos_Settings_Master_Item_EggIncubatorAttributes_descriptor;
      }

      public POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes getDefaultInstanceForType() {
        return POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes.getDefaultInstance();
      }

      public POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes build() {
        POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes buildPartial() {
        POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes result = new POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes(this);
        result.incubatorType_ = incubatorType_;
        result.uses_ = uses_;
        result.distanceMultiplier_ = distanceMultiplier_;
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes) {
          return mergeFrom((POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes other) {
        if (other == POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes.getDefaultInstance()) return this;
        if (other.incubatorType_ != 0) {
          setIncubatorTypeValue(other.getIncubatorTypeValue());
        }
        if (other.getUses() != 0) {
          setUses(other.getUses());
        }
        if (other.getDistanceMultiplier() != 0F) {
          setDistanceMultiplier(other.getDistanceMultiplier());
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private int incubatorType_ = 0;
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubatorType incubator_type = 1;</code>
       */
      public int getIncubatorTypeValue() {
        return incubatorType_;
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubatorType incubator_type = 1;</code>
       */
      public Builder setIncubatorTypeValue(int value) {
        incubatorType_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubatorType incubator_type = 1;</code>
       */
      public POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType getIncubatorType() {
        POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType result = POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType.forNumber(incubatorType_);
        return result == null ? POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType.UNRECOGNIZED : result;
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubatorType incubator_type = 1;</code>
       */
      public Builder setIncubatorType(POGOProtos.Inventory.EggIncubatorTypeOuterClass.EggIncubatorType value) {
        if (value == null) {
          throw new NullPointerException();
        }
        
        incubatorType_ = value.getNumber();
        onChanged();
        return this;
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubatorType incubator_type = 1;</code>
       */
      public Builder clearIncubatorType() {
        
        incubatorType_ = 0;
        onChanged();
        return this;
      }

      private int uses_ ;
      /**
       * <code>optional int32 uses = 2;</code>
       */
      public int getUses() {
        return uses_;
      }
      /**
       * <code>optional int32 uses = 2;</code>
       */
      public Builder setUses(int value) {
        
        uses_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional int32 uses = 2;</code>
       */
      public Builder clearUses() {
        
        uses_ = 0;
        onChanged();
        return this;
      }

      private float distanceMultiplier_ ;
      /**
       * <code>optional float distance_multiplier = 3;</code>
       */
      public float getDistanceMultiplier() {
        return distanceMultiplier_;
      }
      /**
       * <code>optional float distance_multiplier = 3;</code>
       */
      public Builder setDistanceMultiplier(float value) {
        
        distanceMultiplier_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional float distance_multiplier = 3;</code>
       */
      public Builder clearDistanceMultiplier() {
        
        distanceMultiplier_ = 0F;
        onChanged();
        return this;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:POGOProtos.Settings.Master.Item.EggIncubatorAttributes)
    }

    // @@protoc_insertion_point(class_scope:POGOProtos.Settings.Master.Item.EggIncubatorAttributes)
    private static final POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes();
    }

    public static POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<EggIncubatorAttributes>
        PARSER = new com.google.protobuf.AbstractParser<EggIncubatorAttributes>() {
      public EggIncubatorAttributes parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new EggIncubatorAttributes(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<EggIncubatorAttributes> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<EggIncubatorAttributes> getParserForType() {
      return PARSER;
    }

    public POGOProtos.Settings.Master.Item.EggIncubatorAttributesOuterClass.EggIncubatorAttributes getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_POGOProtos_Settings_Master_Item_EggIncubatorAttributes_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_POGOProtos_Settings_Master_Item_EggIncubatorAttributes_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n1Settings/Master/Item/EggIncubatorAttri" +
      "butes.proto\022\037POGOProtos.Settings.Master." +
      "Item\032 Inventory/EggIncubatorType.proto\"\203" +
      "\001\n\026EggIncubatorAttributes\022>\n\016incubator_t" +
      "ype\030\001 \001(\0162&.POGOProtos.Inventory.EggIncu" +
      "batorType\022\014\n\004uses\030\002 \001(\005\022\033\n\023distance_mult" +
      "iplier\030\003 \001(\002P\000b\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          POGOProtos.Inventory.EggIncubatorTypeOuterClass.getDescriptor(),
        }, assigner);
    internal_static_POGOProtos_Settings_Master_Item_EggIncubatorAttributes_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_POGOProtos_Settings_Master_Item_EggIncubatorAttributes_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessage.FieldAccessorTable(
        internal_static_POGOProtos_Settings_Master_Item_EggIncubatorAttributes_descriptor,
        new java.lang.String[] { "IncubatorType", "Uses", "DistanceMultiplier", });
    POGOProtos.Inventory.EggIncubatorTypeOuterClass.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
