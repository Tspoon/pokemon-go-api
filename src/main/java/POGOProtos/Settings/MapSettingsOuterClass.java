// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Settings/MapSettings.proto

package POGOProtos.Settings;

public final class MapSettingsOuterClass {
  private MapSettingsOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface MapSettingsOrBuilder extends
      // @@protoc_insertion_point(interface_extends:POGOProtos.Settings.MapSettings)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>optional double pokemon_visible_range = 1;</code>
     */
    double getPokemonVisibleRange();

    /**
     * <code>optional double poke_nav_range_meters = 2;</code>
     */
    double getPokeNavRangeMeters();

    /**
     * <code>optional double encounter_range_meters = 3;</code>
     */
    double getEncounterRangeMeters();

    /**
     * <code>optional float get_map_objects_min_refresh_seconds = 4;</code>
     */
    float getGetMapObjectsMinRefreshSeconds();

    /**
     * <code>optional float get_map_objects_max_refresh_seconds = 5;</code>
     */
    float getGetMapObjectsMaxRefreshSeconds();

    /**
     * <code>optional float get_map_objects_min_distance_meters = 6;</code>
     */
    float getGetMapObjectsMinDistanceMeters();

    /**
     * <code>optional string google_maps_api_key = 7;</code>
     */
    java.lang.String getGoogleMapsApiKey();
    /**
     * <code>optional string google_maps_api_key = 7;</code>
     */
    com.google.protobuf.ByteString
        getGoogleMapsApiKeyBytes();
  }
  /**
   * Protobuf type {@code POGOProtos.Settings.MapSettings}
   */
  public  static final class MapSettings extends
      com.google.protobuf.GeneratedMessage implements
      // @@protoc_insertion_point(message_implements:POGOProtos.Settings.MapSettings)
      MapSettingsOrBuilder {
    // Use MapSettings.newBuilder() to construct.
    private MapSettings(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
    }
    private MapSettings() {
      pokemonVisibleRange_ = 0D;
      pokeNavRangeMeters_ = 0D;
      encounterRangeMeters_ = 0D;
      getMapObjectsMinRefreshSeconds_ = 0F;
      getMapObjectsMaxRefreshSeconds_ = 0F;
      getMapObjectsMinDistanceMeters_ = 0F;
      googleMapsApiKey_ = "";
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private MapSettings(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 9: {

              pokemonVisibleRange_ = input.readDouble();
              break;
            }
            case 17: {

              pokeNavRangeMeters_ = input.readDouble();
              break;
            }
            case 25: {

              encounterRangeMeters_ = input.readDouble();
              break;
            }
            case 37: {

              getMapObjectsMinRefreshSeconds_ = input.readFloat();
              break;
            }
            case 45: {

              getMapObjectsMaxRefreshSeconds_ = input.readFloat();
              break;
            }
            case 53: {

              getMapObjectsMinDistanceMeters_ = input.readFloat();
              break;
            }
            case 58: {
              java.lang.String s = input.readStringRequireUtf8();

              googleMapsApiKey_ = s;
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return POGOProtos.Settings.MapSettingsOuterClass.internal_static_POGOProtos_Settings_MapSettings_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return POGOProtos.Settings.MapSettingsOuterClass.internal_static_POGOProtos_Settings_MapSettings_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              POGOProtos.Settings.MapSettingsOuterClass.MapSettings.class, POGOProtos.Settings.MapSettingsOuterClass.MapSettings.Builder.class);
    }

    public static final int POKEMON_VISIBLE_RANGE_FIELD_NUMBER = 1;
    private double pokemonVisibleRange_;
    /**
     * <code>optional double pokemon_visible_range = 1;</code>
     */
    public double getPokemonVisibleRange() {
      return pokemonVisibleRange_;
    }

    public static final int POKE_NAV_RANGE_METERS_FIELD_NUMBER = 2;
    private double pokeNavRangeMeters_;
    /**
     * <code>optional double poke_nav_range_meters = 2;</code>
     */
    public double getPokeNavRangeMeters() {
      return pokeNavRangeMeters_;
    }

    public static final int ENCOUNTER_RANGE_METERS_FIELD_NUMBER = 3;
    private double encounterRangeMeters_;
    /**
     * <code>optional double encounter_range_meters = 3;</code>
     */
    public double getEncounterRangeMeters() {
      return encounterRangeMeters_;
    }

    public static final int GET_MAP_OBJECTS_MIN_REFRESH_SECONDS_FIELD_NUMBER = 4;
    private float getMapObjectsMinRefreshSeconds_;
    /**
     * <code>optional float get_map_objects_min_refresh_seconds = 4;</code>
     */
    public float getGetMapObjectsMinRefreshSeconds() {
      return getMapObjectsMinRefreshSeconds_;
    }

    public static final int GET_MAP_OBJECTS_MAX_REFRESH_SECONDS_FIELD_NUMBER = 5;
    private float getMapObjectsMaxRefreshSeconds_;
    /**
     * <code>optional float get_map_objects_max_refresh_seconds = 5;</code>
     */
    public float getGetMapObjectsMaxRefreshSeconds() {
      return getMapObjectsMaxRefreshSeconds_;
    }

    public static final int GET_MAP_OBJECTS_MIN_DISTANCE_METERS_FIELD_NUMBER = 6;
    private float getMapObjectsMinDistanceMeters_;
    /**
     * <code>optional float get_map_objects_min_distance_meters = 6;</code>
     */
    public float getGetMapObjectsMinDistanceMeters() {
      return getMapObjectsMinDistanceMeters_;
    }

    public static final int GOOGLE_MAPS_API_KEY_FIELD_NUMBER = 7;
    private volatile java.lang.Object googleMapsApiKey_;
    /**
     * <code>optional string google_maps_api_key = 7;</code>
     */
    public java.lang.String getGoogleMapsApiKey() {
      java.lang.Object ref = googleMapsApiKey_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        googleMapsApiKey_ = s;
        return s;
      }
    }
    /**
     * <code>optional string google_maps_api_key = 7;</code>
     */
    public com.google.protobuf.ByteString
        getGoogleMapsApiKeyBytes() {
      java.lang.Object ref = googleMapsApiKey_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        googleMapsApiKey_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (pokemonVisibleRange_ != 0D) {
        output.writeDouble(1, pokemonVisibleRange_);
      }
      if (pokeNavRangeMeters_ != 0D) {
        output.writeDouble(2, pokeNavRangeMeters_);
      }
      if (encounterRangeMeters_ != 0D) {
        output.writeDouble(3, encounterRangeMeters_);
      }
      if (getMapObjectsMinRefreshSeconds_ != 0F) {
        output.writeFloat(4, getMapObjectsMinRefreshSeconds_);
      }
      if (getMapObjectsMaxRefreshSeconds_ != 0F) {
        output.writeFloat(5, getMapObjectsMaxRefreshSeconds_);
      }
      if (getMapObjectsMinDistanceMeters_ != 0F) {
        output.writeFloat(6, getMapObjectsMinDistanceMeters_);
      }
      if (!getGoogleMapsApiKeyBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessage.writeString(output, 7, googleMapsApiKey_);
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (pokemonVisibleRange_ != 0D) {
        size += com.google.protobuf.CodedOutputStream
          .computeDoubleSize(1, pokemonVisibleRange_);
      }
      if (pokeNavRangeMeters_ != 0D) {
        size += com.google.protobuf.CodedOutputStream
          .computeDoubleSize(2, pokeNavRangeMeters_);
      }
      if (encounterRangeMeters_ != 0D) {
        size += com.google.protobuf.CodedOutputStream
          .computeDoubleSize(3, encounterRangeMeters_);
      }
      if (getMapObjectsMinRefreshSeconds_ != 0F) {
        size += com.google.protobuf.CodedOutputStream
          .computeFloatSize(4, getMapObjectsMinRefreshSeconds_);
      }
      if (getMapObjectsMaxRefreshSeconds_ != 0F) {
        size += com.google.protobuf.CodedOutputStream
          .computeFloatSize(5, getMapObjectsMaxRefreshSeconds_);
      }
      if (getMapObjectsMinDistanceMeters_ != 0F) {
        size += com.google.protobuf.CodedOutputStream
          .computeFloatSize(6, getMapObjectsMinDistanceMeters_);
      }
      if (!getGoogleMapsApiKeyBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessage.computeStringSize(7, googleMapsApiKey_);
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(POGOProtos.Settings.MapSettingsOuterClass.MapSettings prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code POGOProtos.Settings.MapSettings}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:POGOProtos.Settings.MapSettings)
        POGOProtos.Settings.MapSettingsOuterClass.MapSettingsOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return POGOProtos.Settings.MapSettingsOuterClass.internal_static_POGOProtos_Settings_MapSettings_descriptor;
      }

      protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return POGOProtos.Settings.MapSettingsOuterClass.internal_static_POGOProtos_Settings_MapSettings_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                POGOProtos.Settings.MapSettingsOuterClass.MapSettings.class, POGOProtos.Settings.MapSettingsOuterClass.MapSettings.Builder.class);
      }

      // Construct using POGOProtos.Settings.MapSettingsOuterClass.MapSettings.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessage.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        }
      }
      public Builder clear() {
        super.clear();
        pokemonVisibleRange_ = 0D;

        pokeNavRangeMeters_ = 0D;

        encounterRangeMeters_ = 0D;

        getMapObjectsMinRefreshSeconds_ = 0F;

        getMapObjectsMaxRefreshSeconds_ = 0F;

        getMapObjectsMinDistanceMeters_ = 0F;

        googleMapsApiKey_ = "";

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return POGOProtos.Settings.MapSettingsOuterClass.internal_static_POGOProtos_Settings_MapSettings_descriptor;
      }

      public POGOProtos.Settings.MapSettingsOuterClass.MapSettings getDefaultInstanceForType() {
        return POGOProtos.Settings.MapSettingsOuterClass.MapSettings.getDefaultInstance();
      }

      public POGOProtos.Settings.MapSettingsOuterClass.MapSettings build() {
        POGOProtos.Settings.MapSettingsOuterClass.MapSettings result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public POGOProtos.Settings.MapSettingsOuterClass.MapSettings buildPartial() {
        POGOProtos.Settings.MapSettingsOuterClass.MapSettings result = new POGOProtos.Settings.MapSettingsOuterClass.MapSettings(this);
        result.pokemonVisibleRange_ = pokemonVisibleRange_;
        result.pokeNavRangeMeters_ = pokeNavRangeMeters_;
        result.encounterRangeMeters_ = encounterRangeMeters_;
        result.getMapObjectsMinRefreshSeconds_ = getMapObjectsMinRefreshSeconds_;
        result.getMapObjectsMaxRefreshSeconds_ = getMapObjectsMaxRefreshSeconds_;
        result.getMapObjectsMinDistanceMeters_ = getMapObjectsMinDistanceMeters_;
        result.googleMapsApiKey_ = googleMapsApiKey_;
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof POGOProtos.Settings.MapSettingsOuterClass.MapSettings) {
          return mergeFrom((POGOProtos.Settings.MapSettingsOuterClass.MapSettings)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(POGOProtos.Settings.MapSettingsOuterClass.MapSettings other) {
        if (other == POGOProtos.Settings.MapSettingsOuterClass.MapSettings.getDefaultInstance()) return this;
        if (other.getPokemonVisibleRange() != 0D) {
          setPokemonVisibleRange(other.getPokemonVisibleRange());
        }
        if (other.getPokeNavRangeMeters() != 0D) {
          setPokeNavRangeMeters(other.getPokeNavRangeMeters());
        }
        if (other.getEncounterRangeMeters() != 0D) {
          setEncounterRangeMeters(other.getEncounterRangeMeters());
        }
        if (other.getGetMapObjectsMinRefreshSeconds() != 0F) {
          setGetMapObjectsMinRefreshSeconds(other.getGetMapObjectsMinRefreshSeconds());
        }
        if (other.getGetMapObjectsMaxRefreshSeconds() != 0F) {
          setGetMapObjectsMaxRefreshSeconds(other.getGetMapObjectsMaxRefreshSeconds());
        }
        if (other.getGetMapObjectsMinDistanceMeters() != 0F) {
          setGetMapObjectsMinDistanceMeters(other.getGetMapObjectsMinDistanceMeters());
        }
        if (!other.getGoogleMapsApiKey().isEmpty()) {
          googleMapsApiKey_ = other.googleMapsApiKey_;
          onChanged();
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        POGOProtos.Settings.MapSettingsOuterClass.MapSettings parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (POGOProtos.Settings.MapSettingsOuterClass.MapSettings) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private double pokemonVisibleRange_ ;
      /**
       * <code>optional double pokemon_visible_range = 1;</code>
       */
      public double getPokemonVisibleRange() {
        return pokemonVisibleRange_;
      }
      /**
       * <code>optional double pokemon_visible_range = 1;</code>
       */
      public Builder setPokemonVisibleRange(double value) {
        
        pokemonVisibleRange_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional double pokemon_visible_range = 1;</code>
       */
      public Builder clearPokemonVisibleRange() {
        
        pokemonVisibleRange_ = 0D;
        onChanged();
        return this;
      }

      private double pokeNavRangeMeters_ ;
      /**
       * <code>optional double poke_nav_range_meters = 2;</code>
       */
      public double getPokeNavRangeMeters() {
        return pokeNavRangeMeters_;
      }
      /**
       * <code>optional double poke_nav_range_meters = 2;</code>
       */
      public Builder setPokeNavRangeMeters(double value) {
        
        pokeNavRangeMeters_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional double poke_nav_range_meters = 2;</code>
       */
      public Builder clearPokeNavRangeMeters() {
        
        pokeNavRangeMeters_ = 0D;
        onChanged();
        return this;
      }

      private double encounterRangeMeters_ ;
      /**
       * <code>optional double encounter_range_meters = 3;</code>
       */
      public double getEncounterRangeMeters() {
        return encounterRangeMeters_;
      }
      /**
       * <code>optional double encounter_range_meters = 3;</code>
       */
      public Builder setEncounterRangeMeters(double value) {
        
        encounterRangeMeters_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional double encounter_range_meters = 3;</code>
       */
      public Builder clearEncounterRangeMeters() {
        
        encounterRangeMeters_ = 0D;
        onChanged();
        return this;
      }

      private float getMapObjectsMinRefreshSeconds_ ;
      /**
       * <code>optional float get_map_objects_min_refresh_seconds = 4;</code>
       */
      public float getGetMapObjectsMinRefreshSeconds() {
        return getMapObjectsMinRefreshSeconds_;
      }
      /**
       * <code>optional float get_map_objects_min_refresh_seconds = 4;</code>
       */
      public Builder setGetMapObjectsMinRefreshSeconds(float value) {
        
        getMapObjectsMinRefreshSeconds_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional float get_map_objects_min_refresh_seconds = 4;</code>
       */
      public Builder clearGetMapObjectsMinRefreshSeconds() {
        
        getMapObjectsMinRefreshSeconds_ = 0F;
        onChanged();
        return this;
      }

      private float getMapObjectsMaxRefreshSeconds_ ;
      /**
       * <code>optional float get_map_objects_max_refresh_seconds = 5;</code>
       */
      public float getGetMapObjectsMaxRefreshSeconds() {
        return getMapObjectsMaxRefreshSeconds_;
      }
      /**
       * <code>optional float get_map_objects_max_refresh_seconds = 5;</code>
       */
      public Builder setGetMapObjectsMaxRefreshSeconds(float value) {
        
        getMapObjectsMaxRefreshSeconds_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional float get_map_objects_max_refresh_seconds = 5;</code>
       */
      public Builder clearGetMapObjectsMaxRefreshSeconds() {
        
        getMapObjectsMaxRefreshSeconds_ = 0F;
        onChanged();
        return this;
      }

      private float getMapObjectsMinDistanceMeters_ ;
      /**
       * <code>optional float get_map_objects_min_distance_meters = 6;</code>
       */
      public float getGetMapObjectsMinDistanceMeters() {
        return getMapObjectsMinDistanceMeters_;
      }
      /**
       * <code>optional float get_map_objects_min_distance_meters = 6;</code>
       */
      public Builder setGetMapObjectsMinDistanceMeters(float value) {
        
        getMapObjectsMinDistanceMeters_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional float get_map_objects_min_distance_meters = 6;</code>
       */
      public Builder clearGetMapObjectsMinDistanceMeters() {
        
        getMapObjectsMinDistanceMeters_ = 0F;
        onChanged();
        return this;
      }

      private java.lang.Object googleMapsApiKey_ = "";
      /**
       * <code>optional string google_maps_api_key = 7;</code>
       */
      public java.lang.String getGoogleMapsApiKey() {
        java.lang.Object ref = googleMapsApiKey_;
        if (!(ref instanceof java.lang.String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          java.lang.String s = bs.toStringUtf8();
          googleMapsApiKey_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>optional string google_maps_api_key = 7;</code>
       */
      public com.google.protobuf.ByteString
          getGoogleMapsApiKeyBytes() {
        java.lang.Object ref = googleMapsApiKey_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          googleMapsApiKey_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>optional string google_maps_api_key = 7;</code>
       */
      public Builder setGoogleMapsApiKey(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        googleMapsApiKey_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional string google_maps_api_key = 7;</code>
       */
      public Builder clearGoogleMapsApiKey() {
        
        googleMapsApiKey_ = getDefaultInstance().getGoogleMapsApiKey();
        onChanged();
        return this;
      }
      /**
       * <code>optional string google_maps_api_key = 7;</code>
       */
      public Builder setGoogleMapsApiKeyBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        googleMapsApiKey_ = value;
        onChanged();
        return this;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:POGOProtos.Settings.MapSettings)
    }

    // @@protoc_insertion_point(class_scope:POGOProtos.Settings.MapSettings)
    private static final POGOProtos.Settings.MapSettingsOuterClass.MapSettings DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new POGOProtos.Settings.MapSettingsOuterClass.MapSettings();
    }

    public static POGOProtos.Settings.MapSettingsOuterClass.MapSettings getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<MapSettings>
        PARSER = new com.google.protobuf.AbstractParser<MapSettings>() {
      public MapSettings parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new MapSettings(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<MapSettings> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<MapSettings> getParserForType() {
      return PARSER;
    }

    public POGOProtos.Settings.MapSettingsOuterClass.MapSettings getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_POGOProtos_Settings_MapSettings_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_POGOProtos_Settings_MapSettings_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\032Settings/MapSettings.proto\022\023POGOProtos" +
      ".Settings\"\217\002\n\013MapSettings\022\035\n\025pokemon_vis" +
      "ible_range\030\001 \001(\001\022\035\n\025poke_nav_range_meter" +
      "s\030\002 \001(\001\022\036\n\026encounter_range_meters\030\003 \001(\001\022" +
      "+\n#get_map_objects_min_refresh_seconds\030\004" +
      " \001(\002\022+\n#get_map_objects_max_refresh_seco" +
      "nds\030\005 \001(\002\022+\n#get_map_objects_min_distanc" +
      "e_meters\030\006 \001(\002\022\033\n\023google_maps_api_key\030\007 " +
      "\001(\tb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_POGOProtos_Settings_MapSettings_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_POGOProtos_Settings_MapSettings_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessage.FieldAccessorTable(
        internal_static_POGOProtos_Settings_MapSettings_descriptor,
        new java.lang.String[] { "PokemonVisibleRange", "PokeNavRangeMeters", "EncounterRangeMeters", "GetMapObjectsMinRefreshSeconds", "GetMapObjectsMaxRefreshSeconds", "GetMapObjectsMinDistanceMeters", "GoogleMapsApiKey", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
