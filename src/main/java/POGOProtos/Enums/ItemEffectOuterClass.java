// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Enums/ItemEffect.proto

package POGOProtos.Enums;

public final class ItemEffectOuterClass {
  private ItemEffectOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  /**
   * Protobuf enum {@code POGOProtos.Enums.ItemEffect}
   */
  public enum ItemEffect
      implements com.google.protobuf.ProtocolMessageEnum {
    /**
     * <code>ITEM_EFFECT_NONE = 0;</code>
     */
    ITEM_EFFECT_NONE(0),
    /**
     * <code>ITEM_EFFECT_CAP_NO_FLEE = 1000;</code>
     */
    ITEM_EFFECT_CAP_NO_FLEE(1000),
    /**
     * <code>ITEM_EFFECT_CAP_NO_MOVEMENT = 1002;</code>
     */
    ITEM_EFFECT_CAP_NO_MOVEMENT(1002),
    /**
     * <code>ITEM_EFFECT_CAP_NO_THREAT = 1003;</code>
     */
    ITEM_EFFECT_CAP_NO_THREAT(1003),
    /**
     * <code>ITEM_EFFECT_CAP_TARGET_MAX = 1004;</code>
     */
    ITEM_EFFECT_CAP_TARGET_MAX(1004),
    /**
     * <code>ITEM_EFFECT_CAP_TARGET_SLOW = 1005;</code>
     */
    ITEM_EFFECT_CAP_TARGET_SLOW(1005),
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_NIGHT = 1006;</code>
     */
    ITEM_EFFECT_CAP_CHANCE_NIGHT(1006),
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_TRAINER = 1007;</code>
     */
    ITEM_EFFECT_CAP_CHANCE_TRAINER(1007),
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_FIRST_THROW = 1008;</code>
     */
    ITEM_EFFECT_CAP_CHANCE_FIRST_THROW(1008),
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_LEGEND = 1009;</code>
     */
    ITEM_EFFECT_CAP_CHANCE_LEGEND(1009),
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_HEAVY = 1010;</code>
     */
    ITEM_EFFECT_CAP_CHANCE_HEAVY(1010),
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_REPEAT = 1011;</code>
     */
    ITEM_EFFECT_CAP_CHANCE_REPEAT(1011),
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_MULTI_THROW = 1012;</code>
     */
    ITEM_EFFECT_CAP_CHANCE_MULTI_THROW(1012),
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_ALWAYS = 1013;</code>
     */
    ITEM_EFFECT_CAP_CHANCE_ALWAYS(1013),
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_SINGLE_THROW = 1014;</code>
     */
    ITEM_EFFECT_CAP_CHANCE_SINGLE_THROW(1014),
    UNRECOGNIZED(-1),
    ;

    /**
     * <code>ITEM_EFFECT_NONE = 0;</code>
     */
    public static final int ITEM_EFFECT_NONE_VALUE = 0;
    /**
     * <code>ITEM_EFFECT_CAP_NO_FLEE = 1000;</code>
     */
    public static final int ITEM_EFFECT_CAP_NO_FLEE_VALUE = 1000;
    /**
     * <code>ITEM_EFFECT_CAP_NO_MOVEMENT = 1002;</code>
     */
    public static final int ITEM_EFFECT_CAP_NO_MOVEMENT_VALUE = 1002;
    /**
     * <code>ITEM_EFFECT_CAP_NO_THREAT = 1003;</code>
     */
    public static final int ITEM_EFFECT_CAP_NO_THREAT_VALUE = 1003;
    /**
     * <code>ITEM_EFFECT_CAP_TARGET_MAX = 1004;</code>
     */
    public static final int ITEM_EFFECT_CAP_TARGET_MAX_VALUE = 1004;
    /**
     * <code>ITEM_EFFECT_CAP_TARGET_SLOW = 1005;</code>
     */
    public static final int ITEM_EFFECT_CAP_TARGET_SLOW_VALUE = 1005;
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_NIGHT = 1006;</code>
     */
    public static final int ITEM_EFFECT_CAP_CHANCE_NIGHT_VALUE = 1006;
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_TRAINER = 1007;</code>
     */
    public static final int ITEM_EFFECT_CAP_CHANCE_TRAINER_VALUE = 1007;
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_FIRST_THROW = 1008;</code>
     */
    public static final int ITEM_EFFECT_CAP_CHANCE_FIRST_THROW_VALUE = 1008;
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_LEGEND = 1009;</code>
     */
    public static final int ITEM_EFFECT_CAP_CHANCE_LEGEND_VALUE = 1009;
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_HEAVY = 1010;</code>
     */
    public static final int ITEM_EFFECT_CAP_CHANCE_HEAVY_VALUE = 1010;
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_REPEAT = 1011;</code>
     */
    public static final int ITEM_EFFECT_CAP_CHANCE_REPEAT_VALUE = 1011;
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_MULTI_THROW = 1012;</code>
     */
    public static final int ITEM_EFFECT_CAP_CHANCE_MULTI_THROW_VALUE = 1012;
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_ALWAYS = 1013;</code>
     */
    public static final int ITEM_EFFECT_CAP_CHANCE_ALWAYS_VALUE = 1013;
    /**
     * <code>ITEM_EFFECT_CAP_CHANCE_SINGLE_THROW = 1014;</code>
     */
    public static final int ITEM_EFFECT_CAP_CHANCE_SINGLE_THROW_VALUE = 1014;


    public final int getNumber() {
      if (this == UNRECOGNIZED) {
        throw new java.lang.IllegalArgumentException(
            "Can't get the number of an unknown enum value.");
      }
      return value;
    }

    /**
     * @deprecated Use {@link #forNumber(int)} instead.
     */
    @java.lang.Deprecated
    public static ItemEffect valueOf(int value) {
      return forNumber(value);
    }

    public static ItemEffect forNumber(int value) {
      switch (value) {
        case 0: return ITEM_EFFECT_NONE;
        case 1000: return ITEM_EFFECT_CAP_NO_FLEE;
        case 1002: return ITEM_EFFECT_CAP_NO_MOVEMENT;
        case 1003: return ITEM_EFFECT_CAP_NO_THREAT;
        case 1004: return ITEM_EFFECT_CAP_TARGET_MAX;
        case 1005: return ITEM_EFFECT_CAP_TARGET_SLOW;
        case 1006: return ITEM_EFFECT_CAP_CHANCE_NIGHT;
        case 1007: return ITEM_EFFECT_CAP_CHANCE_TRAINER;
        case 1008: return ITEM_EFFECT_CAP_CHANCE_FIRST_THROW;
        case 1009: return ITEM_EFFECT_CAP_CHANCE_LEGEND;
        case 1010: return ITEM_EFFECT_CAP_CHANCE_HEAVY;
        case 1011: return ITEM_EFFECT_CAP_CHANCE_REPEAT;
        case 1012: return ITEM_EFFECT_CAP_CHANCE_MULTI_THROW;
        case 1013: return ITEM_EFFECT_CAP_CHANCE_ALWAYS;
        case 1014: return ITEM_EFFECT_CAP_CHANCE_SINGLE_THROW;
        default: return null;
      }
    }

    public static com.google.protobuf.Internal.EnumLiteMap<ItemEffect>
        internalGetValueMap() {
      return internalValueMap;
    }
    private static final com.google.protobuf.Internal.EnumLiteMap<
        ItemEffect> internalValueMap =
          new com.google.protobuf.Internal.EnumLiteMap<ItemEffect>() {
            public ItemEffect findValueByNumber(int number) {
              return ItemEffect.forNumber(number);
            }
          };

    public final com.google.protobuf.Descriptors.EnumValueDescriptor
        getValueDescriptor() {
      return getDescriptor().getValues().get(ordinal());
    }
    public final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptorForType() {
      return getDescriptor();
    }
    public static final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptor() {
      return POGOProtos.Enums.ItemEffectOuterClass.getDescriptor().getEnumTypes().get(0);
    }

    private static final ItemEffect[] VALUES = values();

    public static ItemEffect valueOf(
        com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
      if (desc.getType() != getDescriptor()) {
        throw new java.lang.IllegalArgumentException(
          "EnumValueDescriptor is not for this type.");
      }
      if (desc.getIndex() == -1) {
        return UNRECOGNIZED;
      }
      return VALUES[desc.getIndex()];
    }

    private final int value;

    private ItemEffect(int value) {
      this.value = value;
    }

    // @@protoc_insertion_point(enum_scope:POGOProtos.Enums.ItemEffect)
  }


  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\026Enums/ItemEffect.proto\022\020POGOProtos.Enu" +
      "ms*\230\004\n\nItemEffect\022\024\n\020ITEM_EFFECT_NONE\020\000\022" +
      "\034\n\027ITEM_EFFECT_CAP_NO_FLEE\020\350\007\022 \n\033ITEM_EF" +
      "FECT_CAP_NO_MOVEMENT\020\352\007\022\036\n\031ITEM_EFFECT_C" +
      "AP_NO_THREAT\020\353\007\022\037\n\032ITEM_EFFECT_CAP_TARGE" +
      "T_MAX\020\354\007\022 \n\033ITEM_EFFECT_CAP_TARGET_SLOW\020" +
      "\355\007\022!\n\034ITEM_EFFECT_CAP_CHANCE_NIGHT\020\356\007\022#\n" +
      "\036ITEM_EFFECT_CAP_CHANCE_TRAINER\020\357\007\022\'\n\"IT" +
      "EM_EFFECT_CAP_CHANCE_FIRST_THROW\020\360\007\022\"\n\035I" +
      "TEM_EFFECT_CAP_CHANCE_LEGEND\020\361\007\022!\n\034ITEM_",
      "EFFECT_CAP_CHANCE_HEAVY\020\362\007\022\"\n\035ITEM_EFFEC" +
      "T_CAP_CHANCE_REPEAT\020\363\007\022\'\n\"ITEM_EFFECT_CA" +
      "P_CHANCE_MULTI_THROW\020\364\007\022\"\n\035ITEM_EFFECT_C" +
      "AP_CHANCE_ALWAYS\020\365\007\022(\n#ITEM_EFFECT_CAP_C" +
      "HANCE_SINGLE_THROW\020\366\007b\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
  }

  // @@protoc_insertion_point(outer_class_scope)
}
