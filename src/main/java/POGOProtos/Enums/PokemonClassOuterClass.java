// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Enums/PokemonClass.proto

package POGOProtos.Enums;

public final class PokemonClassOuterClass {
  private PokemonClassOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  /**
   * Protobuf enum {@code POGOProtos.Enums.PokemonClass}
   */
  public enum PokemonClass
      implements com.google.protobuf.ProtocolMessageEnum {
    /**
     * <code>NORMAL = 0;</code>
     */
    NORMAL(0),
    /**
     * <code>LEGENDARY = 1;</code>
     */
    LEGENDARY(1),
    /**
     * <code>MYTHIC = 2;</code>
     */
    MYTHIC(2),
    UNRECOGNIZED(-1),
    ;

    /**
     * <code>NORMAL = 0;</code>
     */
    public static final int NORMAL_VALUE = 0;
    /**
     * <code>LEGENDARY = 1;</code>
     */
    public static final int LEGENDARY_VALUE = 1;
    /**
     * <code>MYTHIC = 2;</code>
     */
    public static final int MYTHIC_VALUE = 2;


    public final int getNumber() {
      if (this == UNRECOGNIZED) {
        throw new java.lang.IllegalArgumentException(
            "Can't get the number of an unknown enum value.");
      }
      return value;
    }

    /**
     * @deprecated Use {@link #forNumber(int)} instead.
     */
    @java.lang.Deprecated
    public static PokemonClass valueOf(int value) {
      return forNumber(value);
    }

    public static PokemonClass forNumber(int value) {
      switch (value) {
        case 0: return NORMAL;
        case 1: return LEGENDARY;
        case 2: return MYTHIC;
        default: return null;
      }
    }

    public static com.google.protobuf.Internal.EnumLiteMap<PokemonClass>
        internalGetValueMap() {
      return internalValueMap;
    }
    private static final com.google.protobuf.Internal.EnumLiteMap<
        PokemonClass> internalValueMap =
          new com.google.protobuf.Internal.EnumLiteMap<PokemonClass>() {
            public PokemonClass findValueByNumber(int number) {
              return PokemonClass.forNumber(number);
            }
          };

    public final com.google.protobuf.Descriptors.EnumValueDescriptor
        getValueDescriptor() {
      return getDescriptor().getValues().get(ordinal());
    }
    public final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptorForType() {
      return getDescriptor();
    }
    public static final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptor() {
      return POGOProtos.Enums.PokemonClassOuterClass.getDescriptor().getEnumTypes().get(0);
    }

    private static final PokemonClass[] VALUES = values();

    public static PokemonClass valueOf(
        com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
      if (desc.getType() != getDescriptor()) {
        throw new java.lang.IllegalArgumentException(
          "EnumValueDescriptor is not for this type.");
      }
      if (desc.getIndex() == -1) {
        return UNRECOGNIZED;
      }
      return VALUES[desc.getIndex()];
    }

    private final int value;

    private PokemonClass(int value) {
      this.value = value;
    }

    // @@protoc_insertion_point(enum_scope:POGOProtos.Enums.PokemonClass)
  }


  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\030Enums/PokemonClass.proto\022\020POGOProtos.E" +
      "nums*5\n\014PokemonClass\022\n\n\006NORMAL\020\000\022\r\n\tLEGE" +
      "NDARY\020\001\022\n\n\006MYTHIC\020\002b\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
  }

  // @@protoc_insertion_point(outer_class_scope)
}
