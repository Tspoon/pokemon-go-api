// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Enums/IapItemCategory.proto

package POGOProtos.Enums;

public final class IapItemCategory {
  private IapItemCategory() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  /**
   * Protobuf enum {@code POGOProtos.Enums.HoloIapItemCategory}
   */
  public enum HoloIapItemCategory
      implements com.google.protobuf.ProtocolMessageEnum {
    /**
     * <code>IAP_CATEGORY_NONE = 0;</code>
     */
    IAP_CATEGORY_NONE(0),
    /**
     * <code>IAP_CATEGORY_BUNDLE = 1;</code>
     */
    IAP_CATEGORY_BUNDLE(1),
    /**
     * <code>IAP_CATEGORY_ITEMS = 2;</code>
     */
    IAP_CATEGORY_ITEMS(2),
    /**
     * <code>IAP_CATEGORY_UPGRADES = 3;</code>
     */
    IAP_CATEGORY_UPGRADES(3),
    /**
     * <code>IAP_CATEGORY_POKECOINS = 4;</code>
     */
    IAP_CATEGORY_POKECOINS(4),
    UNRECOGNIZED(-1),
    ;

    /**
     * <code>IAP_CATEGORY_NONE = 0;</code>
     */
    public static final int IAP_CATEGORY_NONE_VALUE = 0;
    /**
     * <code>IAP_CATEGORY_BUNDLE = 1;</code>
     */
    public static final int IAP_CATEGORY_BUNDLE_VALUE = 1;
    /**
     * <code>IAP_CATEGORY_ITEMS = 2;</code>
     */
    public static final int IAP_CATEGORY_ITEMS_VALUE = 2;
    /**
     * <code>IAP_CATEGORY_UPGRADES = 3;</code>
     */
    public static final int IAP_CATEGORY_UPGRADES_VALUE = 3;
    /**
     * <code>IAP_CATEGORY_POKECOINS = 4;</code>
     */
    public static final int IAP_CATEGORY_POKECOINS_VALUE = 4;


    public final int getNumber() {
      if (this == UNRECOGNIZED) {
        throw new java.lang.IllegalArgumentException(
            "Can't get the number of an unknown enum value.");
      }
      return value;
    }

    /**
     * @deprecated Use {@link #forNumber(int)} instead.
     */
    @java.lang.Deprecated
    public static HoloIapItemCategory valueOf(int value) {
      return forNumber(value);
    }

    public static HoloIapItemCategory forNumber(int value) {
      switch (value) {
        case 0: return IAP_CATEGORY_NONE;
        case 1: return IAP_CATEGORY_BUNDLE;
        case 2: return IAP_CATEGORY_ITEMS;
        case 3: return IAP_CATEGORY_UPGRADES;
        case 4: return IAP_CATEGORY_POKECOINS;
        default: return null;
      }
    }

    public static com.google.protobuf.Internal.EnumLiteMap<HoloIapItemCategory>
        internalGetValueMap() {
      return internalValueMap;
    }
    private static final com.google.protobuf.Internal.EnumLiteMap<
        HoloIapItemCategory> internalValueMap =
          new com.google.protobuf.Internal.EnumLiteMap<HoloIapItemCategory>() {
            public HoloIapItemCategory findValueByNumber(int number) {
              return HoloIapItemCategory.forNumber(number);
            }
          };

    public final com.google.protobuf.Descriptors.EnumValueDescriptor
        getValueDescriptor() {
      return getDescriptor().getValues().get(ordinal());
    }
    public final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptorForType() {
      return getDescriptor();
    }
    public static final com.google.protobuf.Descriptors.EnumDescriptor
        getDescriptor() {
      return POGOProtos.Enums.IapItemCategory.getDescriptor().getEnumTypes().get(0);
    }

    private static final HoloIapItemCategory[] VALUES = values();

    public static HoloIapItemCategory valueOf(
        com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
      if (desc.getType() != getDescriptor()) {
        throw new java.lang.IllegalArgumentException(
          "EnumValueDescriptor is not for this type.");
      }
      if (desc.getIndex() == -1) {
        return UNRECOGNIZED;
      }
      return VALUES[desc.getIndex()];
    }

    private final int value;

    private HoloIapItemCategory(int value) {
      this.value = value;
    }

    // @@protoc_insertion_point(enum_scope:POGOProtos.Enums.HoloIapItemCategory)
  }


  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\033Enums/IapItemCategory.proto\022\020POGOProto" +
      "s.Enums*\224\001\n\023HoloIapItemCategory\022\025\n\021IAP_C" +
      "ATEGORY_NONE\020\000\022\027\n\023IAP_CATEGORY_BUNDLE\020\001\022" +
      "\026\n\022IAP_CATEGORY_ITEMS\020\002\022\031\n\025IAP_CATEGORY_" +
      "UPGRADES\020\003\022\032\n\026IAP_CATEGORY_POKECOINS\020\004b\006" +
      "proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
  }

  // @@protoc_insertion_point(outer_class_scope)
}
