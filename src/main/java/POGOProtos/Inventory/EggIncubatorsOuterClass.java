// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Inventory/EggIncubators.proto

package POGOProtos.Inventory;

public final class EggIncubatorsOuterClass {
  private EggIncubatorsOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface EggIncubatorsOrBuilder extends
      // @@protoc_insertion_point(interface_extends:POGOProtos.Inventory.EggIncubators)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
     */
    boolean hasEggIncubator();
    /**
     * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
     */
    POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator getEggIncubator();
    /**
     * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
     */
    POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubatorOrBuilder getEggIncubatorOrBuilder();
  }
  /**
   * Protobuf type {@code POGOProtos.Inventory.EggIncubators}
   */
  public  static final class EggIncubators extends
      com.google.protobuf.GeneratedMessage implements
      // @@protoc_insertion_point(message_implements:POGOProtos.Inventory.EggIncubators)
      EggIncubatorsOrBuilder {
    // Use EggIncubators.newBuilder() to construct.
    private EggIncubators(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
    }
    private EggIncubators() {
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private EggIncubators(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 10: {
              POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.Builder subBuilder = null;
              if (eggIncubator_ != null) {
                subBuilder = eggIncubator_.toBuilder();
              }
              eggIncubator_ = input.readMessage(POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.parser(), extensionRegistry);
              if (subBuilder != null) {
                subBuilder.mergeFrom(eggIncubator_);
                eggIncubator_ = subBuilder.buildPartial();
              }

              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return POGOProtos.Inventory.EggIncubatorsOuterClass.internal_static_POGOProtos_Inventory_EggIncubators_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return POGOProtos.Inventory.EggIncubatorsOuterClass.internal_static_POGOProtos_Inventory_EggIncubators_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators.class, POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators.Builder.class);
    }

    public static final int EGG_INCUBATOR_FIELD_NUMBER = 1;
    private POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator eggIncubator_;
    /**
     * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
     */
    public boolean hasEggIncubator() {
      return eggIncubator_ != null;
    }
    /**
     * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
     */
    public POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator getEggIncubator() {
      return eggIncubator_ == null ? POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.getDefaultInstance() : eggIncubator_;
    }
    /**
     * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
     */
    public POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubatorOrBuilder getEggIncubatorOrBuilder() {
      return getEggIncubator();
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (eggIncubator_ != null) {
        output.writeMessage(1, getEggIncubator());
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (eggIncubator_ != null) {
        size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(1, getEggIncubator());
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code POGOProtos.Inventory.EggIncubators}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:POGOProtos.Inventory.EggIncubators)
        POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubatorsOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return POGOProtos.Inventory.EggIncubatorsOuterClass.internal_static_POGOProtos_Inventory_EggIncubators_descriptor;
      }

      protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return POGOProtos.Inventory.EggIncubatorsOuterClass.internal_static_POGOProtos_Inventory_EggIncubators_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators.class, POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators.Builder.class);
      }

      // Construct using POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessage.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
        }
      }
      public Builder clear() {
        super.clear();
        if (eggIncubatorBuilder_ == null) {
          eggIncubator_ = null;
        } else {
          eggIncubator_ = null;
          eggIncubatorBuilder_ = null;
        }
        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return POGOProtos.Inventory.EggIncubatorsOuterClass.internal_static_POGOProtos_Inventory_EggIncubators_descriptor;
      }

      public POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators getDefaultInstanceForType() {
        return POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators.getDefaultInstance();
      }

      public POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators build() {
        POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators buildPartial() {
        POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators result = new POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators(this);
        if (eggIncubatorBuilder_ == null) {
          result.eggIncubator_ = eggIncubator_;
        } else {
          result.eggIncubator_ = eggIncubatorBuilder_.build();
        }
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators) {
          return mergeFrom((POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators other) {
        if (other == POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators.getDefaultInstance()) return this;
        if (other.hasEggIncubator()) {
          mergeEggIncubator(other.getEggIncubator());
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator eggIncubator_ = null;
      private com.google.protobuf.SingleFieldBuilder<
          POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator, POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.Builder, POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubatorOrBuilder> eggIncubatorBuilder_;
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
       */
      public boolean hasEggIncubator() {
        return eggIncubatorBuilder_ != null || eggIncubator_ != null;
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
       */
      public POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator getEggIncubator() {
        if (eggIncubatorBuilder_ == null) {
          return eggIncubator_ == null ? POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.getDefaultInstance() : eggIncubator_;
        } else {
          return eggIncubatorBuilder_.getMessage();
        }
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
       */
      public Builder setEggIncubator(POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator value) {
        if (eggIncubatorBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          eggIncubator_ = value;
          onChanged();
        } else {
          eggIncubatorBuilder_.setMessage(value);
        }

        return this;
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
       */
      public Builder setEggIncubator(
          POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.Builder builderForValue) {
        if (eggIncubatorBuilder_ == null) {
          eggIncubator_ = builderForValue.build();
          onChanged();
        } else {
          eggIncubatorBuilder_.setMessage(builderForValue.build());
        }

        return this;
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
       */
      public Builder mergeEggIncubator(POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator value) {
        if (eggIncubatorBuilder_ == null) {
          if (eggIncubator_ != null) {
            eggIncubator_ =
              POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.newBuilder(eggIncubator_).mergeFrom(value).buildPartial();
          } else {
            eggIncubator_ = value;
          }
          onChanged();
        } else {
          eggIncubatorBuilder_.mergeFrom(value);
        }

        return this;
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
       */
      public Builder clearEggIncubator() {
        if (eggIncubatorBuilder_ == null) {
          eggIncubator_ = null;
          onChanged();
        } else {
          eggIncubator_ = null;
          eggIncubatorBuilder_ = null;
        }

        return this;
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
       */
      public POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.Builder getEggIncubatorBuilder() {
        
        onChanged();
        return getEggIncubatorFieldBuilder().getBuilder();
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
       */
      public POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubatorOrBuilder getEggIncubatorOrBuilder() {
        if (eggIncubatorBuilder_ != null) {
          return eggIncubatorBuilder_.getMessageOrBuilder();
        } else {
          return eggIncubator_ == null ?
              POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.getDefaultInstance() : eggIncubator_;
        }
      }
      /**
       * <code>optional .POGOProtos.Inventory.EggIncubator egg_incubator = 1;</code>
       */
      private com.google.protobuf.SingleFieldBuilder<
          POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator, POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.Builder, POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubatorOrBuilder> 
          getEggIncubatorFieldBuilder() {
        if (eggIncubatorBuilder_ == null) {
          eggIncubatorBuilder_ = new com.google.protobuf.SingleFieldBuilder<
              POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator, POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubator.Builder, POGOProtos.Inventory.EggIncubatorOuterClass.EggIncubatorOrBuilder>(
                  getEggIncubator(),
                  getParentForChildren(),
                  isClean());
          eggIncubator_ = null;
        }
        return eggIncubatorBuilder_;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:POGOProtos.Inventory.EggIncubators)
    }

    // @@protoc_insertion_point(class_scope:POGOProtos.Inventory.EggIncubators)
    private static final POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators();
    }

    public static POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<EggIncubators>
        PARSER = new com.google.protobuf.AbstractParser<EggIncubators>() {
      public EggIncubators parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new EggIncubators(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<EggIncubators> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<EggIncubators> getParserForType() {
      return PARSER;
    }

    public POGOProtos.Inventory.EggIncubatorsOuterClass.EggIncubators getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_POGOProtos_Inventory_EggIncubators_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_POGOProtos_Inventory_EggIncubators_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\035Inventory/EggIncubators.proto\022\024POGOPro" +
      "tos.Inventory\032\034Inventory/EggIncubator.pr" +
      "oto\"J\n\rEggIncubators\0229\n\regg_incubator\030\001 " +
      "\001(\0132\".POGOProtos.Inventory.EggIncubatorP" +
      "\000b\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          POGOProtos.Inventory.EggIncubatorOuterClass.getDescriptor(),
        }, assigner);
    internal_static_POGOProtos_Inventory_EggIncubators_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_POGOProtos_Inventory_EggIncubators_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessage.FieldAccessorTable(
        internal_static_POGOProtos_Inventory_EggIncubators_descriptor,
        new java.lang.String[] { "EggIncubator", });
    POGOProtos.Inventory.EggIncubatorOuterClass.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
