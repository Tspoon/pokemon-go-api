// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Inventory/InventoryDelta.proto

package POGOProtos.Inventory;

public final class InventoryDeltaOuterClass {
  private InventoryDeltaOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface InventoryDeltaOrBuilder extends
      // @@protoc_insertion_point(interface_extends:POGOProtos.Inventory.InventoryDelta)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>optional int64 original_timestamp_ms = 1;</code>
     */
    long getOriginalTimestampMs();

    /**
     * <code>optional int64 new_timestamp_ms = 2;</code>
     */
    long getNewTimestampMs();

    /**
     * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
     */
    java.util.List<POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem> 
        getInventoryItemsList();
    /**
     * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
     */
    POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem getInventoryItems(int index);
    /**
     * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
     */
    int getInventoryItemsCount();
    /**
     * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
     */
    java.util.List<? extends POGOProtos.Inventory.InventoryItemOuterClass.InventoryItemOrBuilder> 
        getInventoryItemsOrBuilderList();
    /**
     * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
     */
    POGOProtos.Inventory.InventoryItemOuterClass.InventoryItemOrBuilder getInventoryItemsOrBuilder(
        int index);
  }
  /**
   * Protobuf type {@code POGOProtos.Inventory.InventoryDelta}
   */
  public  static final class InventoryDelta extends
      com.google.protobuf.GeneratedMessage implements
      // @@protoc_insertion_point(message_implements:POGOProtos.Inventory.InventoryDelta)
      InventoryDeltaOrBuilder {
    // Use InventoryDelta.newBuilder() to construct.
    private InventoryDelta(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
    }
    private InventoryDelta() {
      originalTimestampMs_ = 0L;
      newTimestampMs_ = 0L;
      inventoryItems_ = java.util.Collections.emptyList();
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private InventoryDelta(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 8: {

              originalTimestampMs_ = input.readInt64();
              break;
            }
            case 16: {

              newTimestampMs_ = input.readInt64();
              break;
            }
            case 26: {
              if (!((mutable_bitField0_ & 0x00000004) == 0x00000004)) {
                inventoryItems_ = new java.util.ArrayList<POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem>();
                mutable_bitField0_ |= 0x00000004;
              }
              inventoryItems_.add(input.readMessage(POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.parser(), extensionRegistry));
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        if (((mutable_bitField0_ & 0x00000004) == 0x00000004)) {
          inventoryItems_ = java.util.Collections.unmodifiableList(inventoryItems_);
        }
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return POGOProtos.Inventory.InventoryDeltaOuterClass.internal_static_POGOProtos_Inventory_InventoryDelta_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return POGOProtos.Inventory.InventoryDeltaOuterClass.internal_static_POGOProtos_Inventory_InventoryDelta_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta.class, POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta.Builder.class);
    }

    private int bitField0_;
    public static final int ORIGINAL_TIMESTAMP_MS_FIELD_NUMBER = 1;
    private long originalTimestampMs_;
    /**
     * <code>optional int64 original_timestamp_ms = 1;</code>
     */
    public long getOriginalTimestampMs() {
      return originalTimestampMs_;
    }

    public static final int NEW_TIMESTAMP_MS_FIELD_NUMBER = 2;
    private long newTimestampMs_;
    /**
     * <code>optional int64 new_timestamp_ms = 2;</code>
     */
    public long getNewTimestampMs() {
      return newTimestampMs_;
    }

    public static final int INVENTORY_ITEMS_FIELD_NUMBER = 3;
    private java.util.List<POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem> inventoryItems_;
    /**
     * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
     */
    public java.util.List<POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem> getInventoryItemsList() {
      return inventoryItems_;
    }
    /**
     * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
     */
    public java.util.List<? extends POGOProtos.Inventory.InventoryItemOuterClass.InventoryItemOrBuilder> 
        getInventoryItemsOrBuilderList() {
      return inventoryItems_;
    }
    /**
     * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
     */
    public int getInventoryItemsCount() {
      return inventoryItems_.size();
    }
    /**
     * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
     */
    public POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem getInventoryItems(int index) {
      return inventoryItems_.get(index);
    }
    /**
     * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
     */
    public POGOProtos.Inventory.InventoryItemOuterClass.InventoryItemOrBuilder getInventoryItemsOrBuilder(
        int index) {
      return inventoryItems_.get(index);
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (originalTimestampMs_ != 0L) {
        output.writeInt64(1, originalTimestampMs_);
      }
      if (newTimestampMs_ != 0L) {
        output.writeInt64(2, newTimestampMs_);
      }
      for (int i = 0; i < inventoryItems_.size(); i++) {
        output.writeMessage(3, inventoryItems_.get(i));
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (originalTimestampMs_ != 0L) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(1, originalTimestampMs_);
      }
      if (newTimestampMs_ != 0L) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(2, newTimestampMs_);
      }
      for (int i = 0; i < inventoryItems_.size(); i++) {
        size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(3, inventoryItems_.get(i));
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code POGOProtos.Inventory.InventoryDelta}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:POGOProtos.Inventory.InventoryDelta)
        POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDeltaOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return POGOProtos.Inventory.InventoryDeltaOuterClass.internal_static_POGOProtos_Inventory_InventoryDelta_descriptor;
      }

      protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return POGOProtos.Inventory.InventoryDeltaOuterClass.internal_static_POGOProtos_Inventory_InventoryDelta_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta.class, POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta.Builder.class);
      }

      // Construct using POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessage.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
          getInventoryItemsFieldBuilder();
        }
      }
      public Builder clear() {
        super.clear();
        originalTimestampMs_ = 0L;

        newTimestampMs_ = 0L;

        if (inventoryItemsBuilder_ == null) {
          inventoryItems_ = java.util.Collections.emptyList();
          bitField0_ = (bitField0_ & ~0x00000004);
        } else {
          inventoryItemsBuilder_.clear();
        }
        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return POGOProtos.Inventory.InventoryDeltaOuterClass.internal_static_POGOProtos_Inventory_InventoryDelta_descriptor;
      }

      public POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta getDefaultInstanceForType() {
        return POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta.getDefaultInstance();
      }

      public POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta build() {
        POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta buildPartial() {
        POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta result = new POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta(this);
        int from_bitField0_ = bitField0_;
        int to_bitField0_ = 0;
        result.originalTimestampMs_ = originalTimestampMs_;
        result.newTimestampMs_ = newTimestampMs_;
        if (inventoryItemsBuilder_ == null) {
          if (((bitField0_ & 0x00000004) == 0x00000004)) {
            inventoryItems_ = java.util.Collections.unmodifiableList(inventoryItems_);
            bitField0_ = (bitField0_ & ~0x00000004);
          }
          result.inventoryItems_ = inventoryItems_;
        } else {
          result.inventoryItems_ = inventoryItemsBuilder_.build();
        }
        result.bitField0_ = to_bitField0_;
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta) {
          return mergeFrom((POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta other) {
        if (other == POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta.getDefaultInstance()) return this;
        if (other.getOriginalTimestampMs() != 0L) {
          setOriginalTimestampMs(other.getOriginalTimestampMs());
        }
        if (other.getNewTimestampMs() != 0L) {
          setNewTimestampMs(other.getNewTimestampMs());
        }
        if (inventoryItemsBuilder_ == null) {
          if (!other.inventoryItems_.isEmpty()) {
            if (inventoryItems_.isEmpty()) {
              inventoryItems_ = other.inventoryItems_;
              bitField0_ = (bitField0_ & ~0x00000004);
            } else {
              ensureInventoryItemsIsMutable();
              inventoryItems_.addAll(other.inventoryItems_);
            }
            onChanged();
          }
        } else {
          if (!other.inventoryItems_.isEmpty()) {
            if (inventoryItemsBuilder_.isEmpty()) {
              inventoryItemsBuilder_.dispose();
              inventoryItemsBuilder_ = null;
              inventoryItems_ = other.inventoryItems_;
              bitField0_ = (bitField0_ & ~0x00000004);
              inventoryItemsBuilder_ = 
                com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders ?
                   getInventoryItemsFieldBuilder() : null;
            } else {
              inventoryItemsBuilder_.addAllMessages(other.inventoryItems_);
            }
          }
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }
      private int bitField0_;

      private long originalTimestampMs_ ;
      /**
       * <code>optional int64 original_timestamp_ms = 1;</code>
       */
      public long getOriginalTimestampMs() {
        return originalTimestampMs_;
      }
      /**
       * <code>optional int64 original_timestamp_ms = 1;</code>
       */
      public Builder setOriginalTimestampMs(long value) {
        
        originalTimestampMs_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional int64 original_timestamp_ms = 1;</code>
       */
      public Builder clearOriginalTimestampMs() {
        
        originalTimestampMs_ = 0L;
        onChanged();
        return this;
      }

      private long newTimestampMs_ ;
      /**
       * <code>optional int64 new_timestamp_ms = 2;</code>
       */
      public long getNewTimestampMs() {
        return newTimestampMs_;
      }
      /**
       * <code>optional int64 new_timestamp_ms = 2;</code>
       */
      public Builder setNewTimestampMs(long value) {
        
        newTimestampMs_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional int64 new_timestamp_ms = 2;</code>
       */
      public Builder clearNewTimestampMs() {
        
        newTimestampMs_ = 0L;
        onChanged();
        return this;
      }

      private java.util.List<POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem> inventoryItems_ =
        java.util.Collections.emptyList();
      private void ensureInventoryItemsIsMutable() {
        if (!((bitField0_ & 0x00000004) == 0x00000004)) {
          inventoryItems_ = new java.util.ArrayList<POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem>(inventoryItems_);
          bitField0_ |= 0x00000004;
         }
      }

      private com.google.protobuf.RepeatedFieldBuilder<
          POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.Builder, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItemOrBuilder> inventoryItemsBuilder_;

      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public java.util.List<POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem> getInventoryItemsList() {
        if (inventoryItemsBuilder_ == null) {
          return java.util.Collections.unmodifiableList(inventoryItems_);
        } else {
          return inventoryItemsBuilder_.getMessageList();
        }
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public int getInventoryItemsCount() {
        if (inventoryItemsBuilder_ == null) {
          return inventoryItems_.size();
        } else {
          return inventoryItemsBuilder_.getCount();
        }
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem getInventoryItems(int index) {
        if (inventoryItemsBuilder_ == null) {
          return inventoryItems_.get(index);
        } else {
          return inventoryItemsBuilder_.getMessage(index);
        }
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public Builder setInventoryItems(
          int index, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem value) {
        if (inventoryItemsBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          ensureInventoryItemsIsMutable();
          inventoryItems_.set(index, value);
          onChanged();
        } else {
          inventoryItemsBuilder_.setMessage(index, value);
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public Builder setInventoryItems(
          int index, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.Builder builderForValue) {
        if (inventoryItemsBuilder_ == null) {
          ensureInventoryItemsIsMutable();
          inventoryItems_.set(index, builderForValue.build());
          onChanged();
        } else {
          inventoryItemsBuilder_.setMessage(index, builderForValue.build());
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public Builder addInventoryItems(POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem value) {
        if (inventoryItemsBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          ensureInventoryItemsIsMutable();
          inventoryItems_.add(value);
          onChanged();
        } else {
          inventoryItemsBuilder_.addMessage(value);
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public Builder addInventoryItems(
          int index, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem value) {
        if (inventoryItemsBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          ensureInventoryItemsIsMutable();
          inventoryItems_.add(index, value);
          onChanged();
        } else {
          inventoryItemsBuilder_.addMessage(index, value);
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public Builder addInventoryItems(
          POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.Builder builderForValue) {
        if (inventoryItemsBuilder_ == null) {
          ensureInventoryItemsIsMutable();
          inventoryItems_.add(builderForValue.build());
          onChanged();
        } else {
          inventoryItemsBuilder_.addMessage(builderForValue.build());
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public Builder addInventoryItems(
          int index, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.Builder builderForValue) {
        if (inventoryItemsBuilder_ == null) {
          ensureInventoryItemsIsMutable();
          inventoryItems_.add(index, builderForValue.build());
          onChanged();
        } else {
          inventoryItemsBuilder_.addMessage(index, builderForValue.build());
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public Builder addAllInventoryItems(
          java.lang.Iterable<? extends POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem> values) {
        if (inventoryItemsBuilder_ == null) {
          ensureInventoryItemsIsMutable();
          com.google.protobuf.AbstractMessageLite.Builder.addAll(
              values, inventoryItems_);
          onChanged();
        } else {
          inventoryItemsBuilder_.addAllMessages(values);
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public Builder clearInventoryItems() {
        if (inventoryItemsBuilder_ == null) {
          inventoryItems_ = java.util.Collections.emptyList();
          bitField0_ = (bitField0_ & ~0x00000004);
          onChanged();
        } else {
          inventoryItemsBuilder_.clear();
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public Builder removeInventoryItems(int index) {
        if (inventoryItemsBuilder_ == null) {
          ensureInventoryItemsIsMutable();
          inventoryItems_.remove(index);
          onChanged();
        } else {
          inventoryItemsBuilder_.remove(index);
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.Builder getInventoryItemsBuilder(
          int index) {
        return getInventoryItemsFieldBuilder().getBuilder(index);
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public POGOProtos.Inventory.InventoryItemOuterClass.InventoryItemOrBuilder getInventoryItemsOrBuilder(
          int index) {
        if (inventoryItemsBuilder_ == null) {
          return inventoryItems_.get(index);  } else {
          return inventoryItemsBuilder_.getMessageOrBuilder(index);
        }
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public java.util.List<? extends POGOProtos.Inventory.InventoryItemOuterClass.InventoryItemOrBuilder> 
           getInventoryItemsOrBuilderList() {
        if (inventoryItemsBuilder_ != null) {
          return inventoryItemsBuilder_.getMessageOrBuilderList();
        } else {
          return java.util.Collections.unmodifiableList(inventoryItems_);
        }
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.Builder addInventoryItemsBuilder() {
        return getInventoryItemsFieldBuilder().addBuilder(
            POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.getDefaultInstance());
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.Builder addInventoryItemsBuilder(
          int index) {
        return getInventoryItemsFieldBuilder().addBuilder(
            index, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.getDefaultInstance());
      }
      /**
       * <code>repeated .POGOProtos.Inventory.InventoryItem inventory_items = 3;</code>
       */
      public java.util.List<POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.Builder> 
           getInventoryItemsBuilderList() {
        return getInventoryItemsFieldBuilder().getBuilderList();
      }
      private com.google.protobuf.RepeatedFieldBuilder<
          POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.Builder, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItemOrBuilder> 
          getInventoryItemsFieldBuilder() {
        if (inventoryItemsBuilder_ == null) {
          inventoryItemsBuilder_ = new com.google.protobuf.RepeatedFieldBuilder<
              POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItem.Builder, POGOProtos.Inventory.InventoryItemOuterClass.InventoryItemOrBuilder>(
                  inventoryItems_,
                  ((bitField0_ & 0x00000004) == 0x00000004),
                  getParentForChildren(),
                  isClean());
          inventoryItems_ = null;
        }
        return inventoryItemsBuilder_;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:POGOProtos.Inventory.InventoryDelta)
    }

    // @@protoc_insertion_point(class_scope:POGOProtos.Inventory.InventoryDelta)
    private static final POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta();
    }

    public static POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<InventoryDelta>
        PARSER = new com.google.protobuf.AbstractParser<InventoryDelta>() {
      public InventoryDelta parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new InventoryDelta(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<InventoryDelta> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<InventoryDelta> getParserForType() {
      return PARSER;
    }

    public POGOProtos.Inventory.InventoryDeltaOuterClass.InventoryDelta getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_POGOProtos_Inventory_InventoryDelta_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_POGOProtos_Inventory_InventoryDelta_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\036Inventory/InventoryDelta.proto\022\024POGOPr" +
      "otos.Inventory\032\035Inventory/InventoryItem." +
      "proto\"\207\001\n\016InventoryDelta\022\035\n\025original_tim" +
      "estamp_ms\030\001 \001(\003\022\030\n\020new_timestamp_ms\030\002 \001(" +
      "\003\022<\n\017inventory_items\030\003 \003(\0132#.POGOProtos." +
      "Inventory.InventoryItemP\000b\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          POGOProtos.Inventory.InventoryItemOuterClass.getDescriptor(),
        }, assigner);
    internal_static_POGOProtos_Inventory_InventoryDelta_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_POGOProtos_Inventory_InventoryDelta_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessage.FieldAccessorTable(
        internal_static_POGOProtos_Inventory_InventoryDelta_descriptor,
        new java.lang.String[] { "OriginalTimestampMs", "NewTimestampMs", "InventoryItems", });
    POGOProtos.Inventory.InventoryItemOuterClass.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
