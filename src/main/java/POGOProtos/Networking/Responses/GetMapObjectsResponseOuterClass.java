// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Networking/Responses/GetMapObjectsResponse.proto

package POGOProtos.Networking.Responses;

public final class GetMapObjectsResponseOuterClass {
  private GetMapObjectsResponseOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
  }
  public interface GetMapObjectsResponseOrBuilder extends
      // @@protoc_insertion_point(interface_extends:POGOProtos.Networking.Responses.GetMapObjectsResponse)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
     */
    java.util.List<POGOProtos.Map.MapCellOuterClass.MapCell> 
        getMapCellsList();
    /**
     * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
     */
    POGOProtos.Map.MapCellOuterClass.MapCell getMapCells(int index);
    /**
     * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
     */
    int getMapCellsCount();
    /**
     * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
     */
    java.util.List<? extends POGOProtos.Map.MapCellOuterClass.MapCellOrBuilder> 
        getMapCellsOrBuilderList();
    /**
     * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
     */
    POGOProtos.Map.MapCellOuterClass.MapCellOrBuilder getMapCellsOrBuilder(
        int index);

    /**
     * <code>optional .POGOProtos.Map.MapObjectsStatus status = 2;</code>
     */
    int getStatusValue();
    /**
     * <code>optional .POGOProtos.Map.MapObjectsStatus status = 2;</code>
     */
    POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus getStatus();
  }
  /**
   * Protobuf type {@code POGOProtos.Networking.Responses.GetMapObjectsResponse}
   */
  public  static final class GetMapObjectsResponse extends
      com.google.protobuf.GeneratedMessage implements
      // @@protoc_insertion_point(message_implements:POGOProtos.Networking.Responses.GetMapObjectsResponse)
      GetMapObjectsResponseOrBuilder {
    // Use GetMapObjectsResponse.newBuilder() to construct.
    private GetMapObjectsResponse(com.google.protobuf.GeneratedMessage.Builder<?> builder) {
      super(builder);
    }
    private GetMapObjectsResponse() {
      mapCells_ = java.util.Collections.emptyList();
      status_ = 0;
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return com.google.protobuf.UnknownFieldSet.getDefaultInstance();
    }
    private GetMapObjectsResponse(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      int mutable_bitField0_ = 0;
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            default: {
              if (!input.skipField(tag)) {
                done = true;
              }
              break;
            }
            case 10: {
              if (!((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
                mapCells_ = new java.util.ArrayList<POGOProtos.Map.MapCellOuterClass.MapCell>();
                mutable_bitField0_ |= 0x00000001;
              }
              mapCells_.add(input.readMessage(POGOProtos.Map.MapCellOuterClass.MapCell.parser(), extensionRegistry));
              break;
            }
            case 16: {
              int rawValue = input.readEnum();

              status_ = rawValue;
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        if (((mutable_bitField0_ & 0x00000001) == 0x00000001)) {
          mapCells_ = java.util.Collections.unmodifiableList(mapCells_);
        }
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.internal_static_POGOProtos_Networking_Responses_GetMapObjectsResponse_descriptor;
    }

    protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.internal_static_POGOProtos_Networking_Responses_GetMapObjectsResponse_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse.class, POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse.Builder.class);
    }

    private int bitField0_;
    public static final int MAP_CELLS_FIELD_NUMBER = 1;
    private java.util.List<POGOProtos.Map.MapCellOuterClass.MapCell> mapCells_;
    /**
     * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
     */
    public java.util.List<POGOProtos.Map.MapCellOuterClass.MapCell> getMapCellsList() {
      return mapCells_;
    }
    /**
     * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
     */
    public java.util.List<? extends POGOProtos.Map.MapCellOuterClass.MapCellOrBuilder> 
        getMapCellsOrBuilderList() {
      return mapCells_;
    }
    /**
     * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
     */
    public int getMapCellsCount() {
      return mapCells_.size();
    }
    /**
     * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
     */
    public POGOProtos.Map.MapCellOuterClass.MapCell getMapCells(int index) {
      return mapCells_.get(index);
    }
    /**
     * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
     */
    public POGOProtos.Map.MapCellOuterClass.MapCellOrBuilder getMapCellsOrBuilder(
        int index) {
      return mapCells_.get(index);
    }

    public static final int STATUS_FIELD_NUMBER = 2;
    private int status_;
    /**
     * <code>optional .POGOProtos.Map.MapObjectsStatus status = 2;</code>
     */
    public int getStatusValue() {
      return status_;
    }
    /**
     * <code>optional .POGOProtos.Map.MapObjectsStatus status = 2;</code>
     */
    public POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus getStatus() {
      POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus result = POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus.forNumber(status_);
      return result == null ? POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus.UNRECOGNIZED : result;
    }

    private byte memoizedIsInitialized = -1;
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      for (int i = 0; i < mapCells_.size(); i++) {
        output.writeMessage(1, mapCells_.get(i));
      }
      if (status_ != POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus.UNSET_STATUS.getNumber()) {
        output.writeEnum(2, status_);
      }
    }

    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      for (int i = 0; i < mapCells_.size(); i++) {
        size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(1, mapCells_.get(i));
      }
      if (status_ != POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus.UNSET_STATUS.getNumber()) {
        size += com.google.protobuf.CodedOutputStream
          .computeEnumSize(2, status_);
      }
      memoizedSize = size;
      return size;
    }

    private static final long serialVersionUID = 0L;
    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input);
    }
    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessage
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessage.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code POGOProtos.Networking.Responses.GetMapObjectsResponse}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessage.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:POGOProtos.Networking.Responses.GetMapObjectsResponse)
        POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponseOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.internal_static_POGOProtos_Networking_Responses_GetMapObjectsResponse_descriptor;
      }

      protected com.google.protobuf.GeneratedMessage.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.internal_static_POGOProtos_Networking_Responses_GetMapObjectsResponse_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse.class, POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse.Builder.class);
      }

      // Construct using POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessage.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders) {
          getMapCellsFieldBuilder();
        }
      }
      public Builder clear() {
        super.clear();
        if (mapCellsBuilder_ == null) {
          mapCells_ = java.util.Collections.emptyList();
          bitField0_ = (bitField0_ & ~0x00000001);
        } else {
          mapCellsBuilder_.clear();
        }
        status_ = 0;

        return this;
      }

      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.internal_static_POGOProtos_Networking_Responses_GetMapObjectsResponse_descriptor;
      }

      public POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse getDefaultInstanceForType() {
        return POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse.getDefaultInstance();
      }

      public POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse build() {
        POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      public POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse buildPartial() {
        POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse result = new POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse(this);
        int from_bitField0_ = bitField0_;
        int to_bitField0_ = 0;
        if (mapCellsBuilder_ == null) {
          if (((bitField0_ & 0x00000001) == 0x00000001)) {
            mapCells_ = java.util.Collections.unmodifiableList(mapCells_);
            bitField0_ = (bitField0_ & ~0x00000001);
          }
          result.mapCells_ = mapCells_;
        } else {
          result.mapCells_ = mapCellsBuilder_.build();
        }
        result.status_ = status_;
        result.bitField0_ = to_bitField0_;
        onBuilt();
        return result;
      }

      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse) {
          return mergeFrom((POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse other) {
        if (other == POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse.getDefaultInstance()) return this;
        if (mapCellsBuilder_ == null) {
          if (!other.mapCells_.isEmpty()) {
            if (mapCells_.isEmpty()) {
              mapCells_ = other.mapCells_;
              bitField0_ = (bitField0_ & ~0x00000001);
            } else {
              ensureMapCellsIsMutable();
              mapCells_.addAll(other.mapCells_);
            }
            onChanged();
          }
        } else {
          if (!other.mapCells_.isEmpty()) {
            if (mapCellsBuilder_.isEmpty()) {
              mapCellsBuilder_.dispose();
              mapCellsBuilder_ = null;
              mapCells_ = other.mapCells_;
              bitField0_ = (bitField0_ & ~0x00000001);
              mapCellsBuilder_ = 
                com.google.protobuf.GeneratedMessage.alwaysUseFieldBuilders ?
                   getMapCellsFieldBuilder() : null;
            } else {
              mapCellsBuilder_.addAllMessages(other.mapCells_);
            }
          }
        }
        if (other.status_ != 0) {
          setStatusValue(other.getStatusValue());
        }
        onChanged();
        return this;
      }

      public final boolean isInitialized() {
        return true;
      }

      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }
      private int bitField0_;

      private java.util.List<POGOProtos.Map.MapCellOuterClass.MapCell> mapCells_ =
        java.util.Collections.emptyList();
      private void ensureMapCellsIsMutable() {
        if (!((bitField0_ & 0x00000001) == 0x00000001)) {
          mapCells_ = new java.util.ArrayList<POGOProtos.Map.MapCellOuterClass.MapCell>(mapCells_);
          bitField0_ |= 0x00000001;
         }
      }

      private com.google.protobuf.RepeatedFieldBuilder<
          POGOProtos.Map.MapCellOuterClass.MapCell, POGOProtos.Map.MapCellOuterClass.MapCell.Builder, POGOProtos.Map.MapCellOuterClass.MapCellOrBuilder> mapCellsBuilder_;

      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public java.util.List<POGOProtos.Map.MapCellOuterClass.MapCell> getMapCellsList() {
        if (mapCellsBuilder_ == null) {
          return java.util.Collections.unmodifiableList(mapCells_);
        } else {
          return mapCellsBuilder_.getMessageList();
        }
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public int getMapCellsCount() {
        if (mapCellsBuilder_ == null) {
          return mapCells_.size();
        } else {
          return mapCellsBuilder_.getCount();
        }
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public POGOProtos.Map.MapCellOuterClass.MapCell getMapCells(int index) {
        if (mapCellsBuilder_ == null) {
          return mapCells_.get(index);
        } else {
          return mapCellsBuilder_.getMessage(index);
        }
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public Builder setMapCells(
          int index, POGOProtos.Map.MapCellOuterClass.MapCell value) {
        if (mapCellsBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          ensureMapCellsIsMutable();
          mapCells_.set(index, value);
          onChanged();
        } else {
          mapCellsBuilder_.setMessage(index, value);
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public Builder setMapCells(
          int index, POGOProtos.Map.MapCellOuterClass.MapCell.Builder builderForValue) {
        if (mapCellsBuilder_ == null) {
          ensureMapCellsIsMutable();
          mapCells_.set(index, builderForValue.build());
          onChanged();
        } else {
          mapCellsBuilder_.setMessage(index, builderForValue.build());
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public Builder addMapCells(POGOProtos.Map.MapCellOuterClass.MapCell value) {
        if (mapCellsBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          ensureMapCellsIsMutable();
          mapCells_.add(value);
          onChanged();
        } else {
          mapCellsBuilder_.addMessage(value);
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public Builder addMapCells(
          int index, POGOProtos.Map.MapCellOuterClass.MapCell value) {
        if (mapCellsBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          ensureMapCellsIsMutable();
          mapCells_.add(index, value);
          onChanged();
        } else {
          mapCellsBuilder_.addMessage(index, value);
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public Builder addMapCells(
          POGOProtos.Map.MapCellOuterClass.MapCell.Builder builderForValue) {
        if (mapCellsBuilder_ == null) {
          ensureMapCellsIsMutable();
          mapCells_.add(builderForValue.build());
          onChanged();
        } else {
          mapCellsBuilder_.addMessage(builderForValue.build());
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public Builder addMapCells(
          int index, POGOProtos.Map.MapCellOuterClass.MapCell.Builder builderForValue) {
        if (mapCellsBuilder_ == null) {
          ensureMapCellsIsMutable();
          mapCells_.add(index, builderForValue.build());
          onChanged();
        } else {
          mapCellsBuilder_.addMessage(index, builderForValue.build());
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public Builder addAllMapCells(
          java.lang.Iterable<? extends POGOProtos.Map.MapCellOuterClass.MapCell> values) {
        if (mapCellsBuilder_ == null) {
          ensureMapCellsIsMutable();
          com.google.protobuf.AbstractMessageLite.Builder.addAll(
              values, mapCells_);
          onChanged();
        } else {
          mapCellsBuilder_.addAllMessages(values);
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public Builder clearMapCells() {
        if (mapCellsBuilder_ == null) {
          mapCells_ = java.util.Collections.emptyList();
          bitField0_ = (bitField0_ & ~0x00000001);
          onChanged();
        } else {
          mapCellsBuilder_.clear();
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public Builder removeMapCells(int index) {
        if (mapCellsBuilder_ == null) {
          ensureMapCellsIsMutable();
          mapCells_.remove(index);
          onChanged();
        } else {
          mapCellsBuilder_.remove(index);
        }
        return this;
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public POGOProtos.Map.MapCellOuterClass.MapCell.Builder getMapCellsBuilder(
          int index) {
        return getMapCellsFieldBuilder().getBuilder(index);
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public POGOProtos.Map.MapCellOuterClass.MapCellOrBuilder getMapCellsOrBuilder(
          int index) {
        if (mapCellsBuilder_ == null) {
          return mapCells_.get(index);  } else {
          return mapCellsBuilder_.getMessageOrBuilder(index);
        }
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public java.util.List<? extends POGOProtos.Map.MapCellOuterClass.MapCellOrBuilder> 
           getMapCellsOrBuilderList() {
        if (mapCellsBuilder_ != null) {
          return mapCellsBuilder_.getMessageOrBuilderList();
        } else {
          return java.util.Collections.unmodifiableList(mapCells_);
        }
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public POGOProtos.Map.MapCellOuterClass.MapCell.Builder addMapCellsBuilder() {
        return getMapCellsFieldBuilder().addBuilder(
            POGOProtos.Map.MapCellOuterClass.MapCell.getDefaultInstance());
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public POGOProtos.Map.MapCellOuterClass.MapCell.Builder addMapCellsBuilder(
          int index) {
        return getMapCellsFieldBuilder().addBuilder(
            index, POGOProtos.Map.MapCellOuterClass.MapCell.getDefaultInstance());
      }
      /**
       * <code>repeated .POGOProtos.Map.MapCell map_cells = 1;</code>
       */
      public java.util.List<POGOProtos.Map.MapCellOuterClass.MapCell.Builder> 
           getMapCellsBuilderList() {
        return getMapCellsFieldBuilder().getBuilderList();
      }
      private com.google.protobuf.RepeatedFieldBuilder<
          POGOProtos.Map.MapCellOuterClass.MapCell, POGOProtos.Map.MapCellOuterClass.MapCell.Builder, POGOProtos.Map.MapCellOuterClass.MapCellOrBuilder> 
          getMapCellsFieldBuilder() {
        if (mapCellsBuilder_ == null) {
          mapCellsBuilder_ = new com.google.protobuf.RepeatedFieldBuilder<
              POGOProtos.Map.MapCellOuterClass.MapCell, POGOProtos.Map.MapCellOuterClass.MapCell.Builder, POGOProtos.Map.MapCellOuterClass.MapCellOrBuilder>(
                  mapCells_,
                  ((bitField0_ & 0x00000001) == 0x00000001),
                  getParentForChildren(),
                  isClean());
          mapCells_ = null;
        }
        return mapCellsBuilder_;
      }

      private int status_ = 0;
      /**
       * <code>optional .POGOProtos.Map.MapObjectsStatus status = 2;</code>
       */
      public int getStatusValue() {
        return status_;
      }
      /**
       * <code>optional .POGOProtos.Map.MapObjectsStatus status = 2;</code>
       */
      public Builder setStatusValue(int value) {
        status_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>optional .POGOProtos.Map.MapObjectsStatus status = 2;</code>
       */
      public POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus getStatus() {
        POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus result = POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus.forNumber(status_);
        return result == null ? POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus.UNRECOGNIZED : result;
      }
      /**
       * <code>optional .POGOProtos.Map.MapObjectsStatus status = 2;</code>
       */
      public Builder setStatus(POGOProtos.Map.MapObjectsStatusOuterClass.MapObjectsStatus value) {
        if (value == null) {
          throw new NullPointerException();
        }
        
        status_ = value.getNumber();
        onChanged();
        return this;
      }
      /**
       * <code>optional .POGOProtos.Map.MapObjectsStatus status = 2;</code>
       */
      public Builder clearStatus() {
        
        status_ = 0;
        onChanged();
        return this;
      }
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }

      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return this;
      }


      // @@protoc_insertion_point(builder_scope:POGOProtos.Networking.Responses.GetMapObjectsResponse)
    }

    // @@protoc_insertion_point(class_scope:POGOProtos.Networking.Responses.GetMapObjectsResponse)
    private static final POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse();
    }

    public static POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<GetMapObjectsResponse>
        PARSER = new com.google.protobuf.AbstractParser<GetMapObjectsResponse>() {
      public GetMapObjectsResponse parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
          return new GetMapObjectsResponse(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<GetMapObjectsResponse> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<GetMapObjectsResponse> getParserForType() {
      return PARSER;
    }

    public POGOProtos.Networking.Responses.GetMapObjectsResponseOuterClass.GetMapObjectsResponse getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_POGOProtos_Networking_Responses_GetMapObjectsResponse_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessage.FieldAccessorTable
      internal_static_POGOProtos_Networking_Responses_GetMapObjectsResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n0Networking/Responses/GetMapObjectsResp" +
      "onse.proto\022\037POGOProtos.Networking.Respon" +
      "ses\032\021Map/MapCell.proto\032\032Map/MapObjectsSt" +
      "atus.proto\"u\n\025GetMapObjectsResponse\022*\n\tm" +
      "ap_cells\030\001 \003(\0132\027.POGOProtos.Map.MapCell\022" +
      "0\n\006status\030\002 \001(\0162 .POGOProtos.Map.MapObje" +
      "ctsStatusb\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          POGOProtos.Map.MapCellOuterClass.getDescriptor(),
          POGOProtos.Map.MapObjectsStatusOuterClass.getDescriptor(),
        }, assigner);
    internal_static_POGOProtos_Networking_Responses_GetMapObjectsResponse_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_POGOProtos_Networking_Responses_GetMapObjectsResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessage.FieldAccessorTable(
        internal_static_POGOProtos_Networking_Responses_GetMapObjectsResponse_descriptor,
        new java.lang.String[] { "MapCells", "Status", });
    POGOProtos.Map.MapCellOuterClass.getDescriptor();
    POGOProtos.Map.MapObjectsStatusOuterClass.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
